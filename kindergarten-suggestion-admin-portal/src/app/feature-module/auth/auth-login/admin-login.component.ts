import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AdminServiceService } from 'src/app/service/admin-service/admin-service.service';
import { PreviousPageService } from 'src/app/service/previous-page/previous-page.service';
@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.scss']
})
export class AdminLoginComponent implements OnInit {
  validateForm!: UntypedFormGroup;
  inputEmailTouched: boolean = false;
  inputPasswordTouched: boolean = false;
  token!: string;
  role!: string;
  errorMessage!: string;
  userName!: string;
  constructor(private fb: UntypedFormBuilder,
    private _adminService: AdminServiceService,
    private _router: Router,
    private _location: Location,
    private _previousPage: PreviousPageService) { }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      email: [null, [Validators.required, Validators.pattern(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)]],
      password: [null, [Validators.required]]

    });
    // Theo dõi sự kiện focus của ô input
    this.email?.valueChanges.subscribe(() => {
      this.inputEmailTouched = true;
    });
    this.password?.valueChanges.subscribe(() => {
      this.inputPasswordTouched = true;
    });
  }
  submitForm(): void {
    this.inputEmailTouched = true;
    this.inputPasswordTouched = true;
    if (this.validateForm.valid) {
      console.log('submit');
    } else {
      Object.values(this.validateForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
    this._adminService.adminLogin(this.validateForm.value).subscribe({
      next: response => {
        this.userName = response.fullName;
        localStorage.setItem('userName', this.userName);
        this.token = response.accessToken;
        localStorage.setItem('accessToken', this.token);
        this.role = response.account.role;
        localStorage.setItem('role', this.role);
        const returnUrl = localStorage.getItem('returnUrl');
        if (!returnUrl) {
          this._router.navigate(['/admin/school']);
        } else {
          this._router.navigateByUrl(returnUrl).then(r => localStorage.removeItem('returnUrl'));
        }
      },
      error: error => {
        this.errorMessage = "Either email address or password is incorrect. Please try again!"
      }
    });
  }
  get email() { return this.validateForm.get('email'); }
  get password() { return this.validateForm.get('password'); }
}
