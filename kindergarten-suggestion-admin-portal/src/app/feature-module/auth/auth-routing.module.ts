import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AdminLoginComponent} from "./auth-login/admin-login.component";
import {ForgotPasswordComponent} from "./auth-forgot-password/forgot-password.component";
import {ResetPasswordComponent} from "./auth-reset-password/reset-password.component";

const routes: Routes = [
  { path: '', pathMatch: "full", redirectTo: 'login'},
  { path: 'login', component: AdminLoginComponent},
  { path: 'forgot-password', component: ForgotPasswordComponent},
  { path: 'reset-password/:token', component: ResetPasswordComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
