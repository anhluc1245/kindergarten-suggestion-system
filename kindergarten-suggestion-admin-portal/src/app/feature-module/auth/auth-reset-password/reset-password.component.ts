import { Component, OnInit } from '@angular/core';
import { FormGroup, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { AdminServiceService } from 'src/app/service/admin-service/admin-service.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  validateForm!: UntypedFormGroup;
  passwordTouched:boolean = false;
  confirmPasswordTouched:boolean = false;
  tokenAccess!: string;
  passwordsMatchChecked: boolean = false;
  constructor(private fb: UntypedFormBuilder,
              private _adminService: AdminServiceService,
              private _router: Router,
              private notification: NzNotificationService,
              private _activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    
    this.tokenAccess = this._activatedRoute.snapshot.params?.['token'];
    this.loadData();
    this.validateForm = this.fb.group({
      password: [null, [Validators.required, Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d).{7,}$/)]],
      confirmPassword: [null, [Validators.required]],
      token:this.tokenAccess
    }, {
      validator: this.passwordMatchValidator,
    });
    // Theo dõi sự kiện focus của ô input
    this.confirmPassword?.valueChanges.subscribe(() => {
      this.confirmPasswordTouched = true;
    });
    this.password?.valueChanges.subscribe(() => {
      this.passwordTouched = true;
    });
  }
  loadData(): void {
    this._adminService.loadResetPage(this.tokenAccess).subscribe({
      next: response =>{
        //FIXME
        console.log("OK");
      },
      error: err => {
        this._router.navigateByUrl('/error/404');
      }
    })
  }
  passwordMatchValidator(formGroup: FormGroup) {

    const password = formGroup.get('password')?.value;
    const confirmPassword = formGroup.get('confirmPassword')?.value;

    if (password !== confirmPassword) {
      formGroup.get('confirmPassword')?.setErrors({ mismatch: true });
    } else {
      formGroup.get('confirmPassword')?.setErrors(null);
    }
  }
  submitForm(): void {
    this.passwordTouched = true;
    this.confirmPasswordTouched = true;
    this.passwordsMatchChecked = true;
    if (this.validateForm.valid) {
      console.log('submit', this.validateForm.value);
    } else {
      Object.values(this.validateForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
    console.log(this.validateForm.value);
    this._adminService.resetPassword(this.validateForm.value).subscribe({
      next: response => {
        this.createNotification('success','Password reset successfully!','You can now log in again');
        setTimeout(() => this.homePage(),2000);
        // localStorage.setItem('resetToken', this.tokenAccess);
      },
      error: error =>{
        this.createNotification('error','Password reset failed','This link has expired. Please go back to Homepage and try again!');
      }
    })
  }
  homePage(): void {
    this._router.navigate(['/admin/auth/login']);

  }
  createNotification(type: string,title:string,content:string): void {
    this.notification.create(type,title,content);
  } 
  get password() { return this.validateForm.get('password'); }
  get confirmPassword() { return this.validateForm.get('confirmPassword'); }
}
