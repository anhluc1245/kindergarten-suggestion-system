import { NgModule } from '@angular/core';


import {RouterModule, Routes} from "@angular/router";
import {SchoolListComponent} from "./school-list/school-list.component";
import {EditSchoolComponent} from "./edit-school/edit-school.component";
import {AddSchoolComponent} from "./add-school/add-school.component";
import {RatingsFeedbacksComponent} from "./ratings/ratings-feedbacks/ratings-feedbacks.component";
import {SchoolDetailComponent} from "./school-detail/school-detail.component";

const routes: Routes = [
  // { path: '', pathMatch: "full", redirectTo: 'list'},
  { path: '', component: SchoolListComponent },
  { path: 'add', component: AddSchoolComponent },
  { path: 'add/:extraPath', redirectTo: 'add', pathMatch: 'prefix' },
  { path: 'edit', pathMatch: "full", redirectTo: 'list' },
  { path: 'edit/:id', component: EditSchoolComponent },
  { path: ':id/rate', component: RatingsFeedbacksComponent},
  { path: ':id', component: SchoolDetailComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SchoolRoutingModule { }
