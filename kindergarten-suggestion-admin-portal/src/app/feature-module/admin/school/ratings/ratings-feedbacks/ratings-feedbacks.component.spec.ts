import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RatingsFeedbacksComponent } from './ratings-feedbacks.component';

describe('RatingsFeedbacksComponent', () => {
  let component: RatingsFeedbacksComponent;
  let fixture: ComponentFixture<RatingsFeedbacksComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RatingsFeedbacksComponent]
    });
    fixture = TestBed.createComponent(RatingsFeedbacksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
