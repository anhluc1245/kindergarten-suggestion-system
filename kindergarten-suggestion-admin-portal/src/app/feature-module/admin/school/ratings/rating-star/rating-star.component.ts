import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-rating-star',
  templateUrl: './rating-star.component.html',
  styleUrls: ['./rating-star.component.scss']
})
export class RatingStarComponent {
  @Input()star!: number;
  @Input() starSize: number = 24; // Giá trị mặc định là 24px

}
