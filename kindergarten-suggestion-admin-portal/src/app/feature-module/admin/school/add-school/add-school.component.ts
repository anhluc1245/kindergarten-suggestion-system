import {Component, Input} from '@angular/core';
import {formatDate} from "@angular/common";
import {finalize} from "rxjs";

@Component({
  selector: 'app-add-school',
  templateUrl: './add-school.component.html',
  styleUrls: ['./add-school.component.scss']
})
export class AddSchoolComponent {
  formData: any;

  constructor( ) {

  }
  handleDataChange(newData: any) {
    this.formData = newData;
    console.log(this.formData);
  }

  protected readonly formatDate = formatDate;
}
