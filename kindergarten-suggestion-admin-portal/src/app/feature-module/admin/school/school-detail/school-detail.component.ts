import {Component, OnInit} from '@angular/core';
import {UntypedFormBuilder, UntypedFormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {AdminSchoolService} from "../../../../service/admin-school-service/admin-school.service";

@Component({
  selector: 'app-school-detail',
  templateUrl: './school-detail.component.html',
  styleUrls: ['./school-detail.component.scss']
})
export class SchoolDetailComponent implements OnInit {
  schoolDetail! : any;
  id : number = 0;
  constructor(private _adminSchoolService : AdminSchoolService,
              private _activatedRoute : ActivatedRoute) {
  }
  ngOnInit(): void {
    // @ts-ignore
    this._activatedRoute.paramMap.subscribe( params => this.id = +params.get('id'))
    this.loadSchoolDetail();
  }

  loadSchoolDetail() {
    this._adminSchoolService.getSchoolDetail(this.id).subscribe({
      next : resp => {
        console.log(resp); // FIXME
        this.schoolDetail = {
          id : resp.id,
          ownerId: resp.ownerId,
          name : resp.name,
          type : resp.type,
          city : resp.city,
          district : resp.district,
          ward : resp.ward,
          street : resp.street,
          email : resp.email,
          website : resp.website,
          phoneNumber : resp.phoneNumber,
          childReceivingAge : resp.childReceivingAge,
          educationMethod : resp.educationMethod,
          minFee : resp.minFee,
          maxFee : resp.maxFee,
          schoolFacilities : resp.schoolFacilities,
          schoolUtilities : resp.schoolUtilities,
          introduction : resp.introduction,
          mainImage : resp.mainImage,
          images : resp.images,
          status: resp.status
        };
      }, error : err => {
        console.error(err)
      }
    })
  }

}
