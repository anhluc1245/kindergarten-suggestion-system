import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RemainderRequestComponent } from './remainder-request.component';

describe('RemaiderRequestComponent', () => {
  let component: RemainderRequestComponent;
  let fixture: ComponentFixture<RemainderRequestComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RemainderRequestComponent]
    });
    fixture = TestBed.createComponent(RemainderRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
