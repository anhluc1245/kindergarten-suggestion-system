import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReminderRoutingModule } from './reminder-routing.module';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzBreadCrumbModule } from 'ng-zorro-antd/breadcrumb';
import { NzPaginationModule } from "ng-zorro-antd/pagination";
import { RemainderRequestComponent } from './remainder-request/remainder-request.component';
import { FormsModule } from '@angular/forms';
import { RequestStatusPipe } from 'src/app/pipe/request-status.pipe';
import {NzTagModule} from "ng-zorro-antd/tag";
import {NzIconModule} from "ng-zorro-antd/icon";




@NgModule({
  declarations: [RemainderRequestComponent],
    imports: [
        CommonModule,
        ReminderRoutingModule,
        NzTableModule,
        NzInputModule,
        NzBreadCrumbModule,
        NzPaginationModule,
        FormsModule,
        RequestStatusPipe,
        NzTagModule,
        NzIconModule

    ]
})
export class ReminderModule { }
