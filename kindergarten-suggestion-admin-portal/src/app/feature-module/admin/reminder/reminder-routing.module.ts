import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RemainderRequestComponent } from './remainder-request/remainder-request.component';
import { DetailRequestComponent } from '../requests-counseling/detail-request/detail-request.component';

const routes: Routes = [
  { path: '', component: RemainderRequestComponent },
  { path: 'detail/:id', component: DetailRequestComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReminderRoutingModule { }
