import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NzBreadCrumbModule } from "ng-zorro-antd/breadcrumb";
import { NzButtonModule } from "ng-zorro-antd/button";
import { NzIconModule } from "ng-zorro-antd/icon";
import { NzInputModule } from "ng-zorro-antd/input";
import { NzMentionModule } from 'ng-zorro-antd/mention';
import { NzPopconfirmModule } from "ng-zorro-antd/popconfirm";
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzTableModule } from 'ng-zorro-antd/table';
import { DobDatePipe } from 'src/app/pipe/dob-date.pipe';
import { RolePipe } from 'src/app/pipe/role.pipe';
import { AddUserComponent } from './add-user/add-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserRoutingModule } from './user-routing.module';
import { ViewListComponent } from './view-list/view-list.component';
import { NzDividerModule } from "ng-zorro-antd/divider";
import { NzCardModule } from "ng-zorro-antd/card";
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzTagModule } from 'ng-zorro-antd/tag';
import {NzPaginationModule} from "ng-zorro-antd/pagination";



@NgModule({
  declarations: [
    ViewListComponent,
    UserDetailComponent,
    AddUserComponent,
    EditUserComponent,
  ],
    imports: [
        CommonModule,
        UserRoutingModule,
        NzTableModule,
        NzPopconfirmModule,
        NzButtonModule,
        NzMentionModule,
        FormsModule,
        NzInputModule,
        NzIconModule,
        NzBreadCrumbModule,
        RolePipe,
        DobDatePipe,
        ReactiveFormsModule,
        NzFormModule,
        NzSelectModule,
        NzCardModule,
        NzDividerModule,
        NzFormModule,
        NzNotificationModule,
        NzTagModule,
        NzPaginationModule
    ],
})
export class UserModule { }
