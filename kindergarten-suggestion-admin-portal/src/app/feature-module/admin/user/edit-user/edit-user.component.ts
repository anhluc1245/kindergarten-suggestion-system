import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { AdminService } from 'src/app/service/admin-service/admin.service';
import { UserService } from 'src/app/service/user-service/user.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {

  updateForm!: FormGroup;
  successMessage: string = "Successfully added!";
  failMessage: string = "Failed!";
  message!: string;
  userId!: number;
  roles: any[] = [];

  constructor(private _formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private _userService: UserService,
    private _adminService: AdminService,
    private notification: NzNotificationService,
    private _router: Router) {
  }


  ngOnInit(): void {

    this.route.params.subscribe(params => {
      console.log(params) //log the entire params object
      this.userId = Number(params['id']);
      this.updateForm = this._formBuilder.group({
        fullName: [null, [Validators.required]],
        email: [null, [Validators.required, Validators.pattern(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)]],
        dob: [null, [Validators.required, this.dateValidator]],
        phoneNumber: [null, [Validators.required, Validators.pattern(/^(0[35789][0-9]{8}|02[0-9]{9})$/)]],
        role: [null, [Validators.required]],
        active: [null],
      });
      this.loadData(Number(params['id']));
      this.getRole();
    });
  }

  loadData(userId: number): void {
    this._userService.getUserDetailById(userId).subscribe({
      next: res => {
        console.log(res);
        console.log(res.dob);
        this.updateForm.setValue({
          fullName: res.fullName,
          email: res.account.email,
          dob: res.dob,
          phoneNumber: res.phoneNumber,
          role: res.account.role,
          active: res.account.active
        });
        if (res.account.active) {
          this.updateForm.controls['active'].setValue("true");
        } else {
          this.updateForm.controls['active'].setValue("false");
        }
      }, error: err => {
        console.log(err);
      }
    });
  }

  submitForm(id: number): void {
    if (this.updateForm.valid) {
      this._adminService.updateUserByAdmin(id, this.updateForm.value).subscribe({
        next: res => {
          this.message = 'Update successfully!'
            this._router.navigateByUrl('/admin/user/list')
              .then(() => this.createNotification('success', this.successMessage, this.message));
        },
        error: err => {
          this.message = err.error;
          this.createNotification('error', this.failMessage, this.message);
        }
      })
    } else {
      Object.values(this.updateForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }

  dateValidator = (control: UntypedFormControl): { [s: string]: boolean } => {

    if (!control.value) {
      return { required: true };
    }
    const currentDate = Date.now();
    const date = new Date(control.value);
    if (date.getTime() >= currentDate) {
      return { confirm: true, error: true };
    }
    return {};
  }

  createNotification(type: string, title: string, message: string): void {
    this.notification.create(
      type,
      title, message
    );
  }

  getRole() {
    this._adminService.getRoleUser().subscribe({
      next: res => {
        console.log(res);
        this.roles = res;
      },
      error: err => { console.log(err); }
    })
  }
}

