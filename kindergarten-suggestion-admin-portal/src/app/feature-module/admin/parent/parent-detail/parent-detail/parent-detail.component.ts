import {AfterViewInit, Component, OnInit} from '@angular/core';
import {NzNotificationService} from "ng-zorro-antd/notification";
import {ActivatedRoute, Router} from "@angular/router";
import {ParentService} from "../../../../../service/parent-service/parent-service.service";
import {FormBuilder, FormGroup} from "@angular/forms";


@Component({
  selector: 'app-parent-detail',
  templateUrl: './parent-detail.component.html',
  styleUrls: ['./parent-detail.component.scss']
})
export class ParentDetailComponent implements OnInit, AfterViewInit {
  role!: string;
  parentForm!: FormGroup;
  parent: any = {}; // Dữ liệu người dùng sẽ được lấy từ API
  parentID: number = 0;
  loadingError: boolean = false;
  school: any = {};
  schoolId! : any;
  constructor(
    private _activeRoute: ActivatedRoute,
    private _parentService: ParentService,
    private _router: Router,
    private notification: NzNotificationService,
    private _formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.schoolId = localStorage.getItem('schoolId');
    console.log(this.schoolId)
    this.parentForm = this._formBuilder.group({
      fullName: [''],
      dob: [''],
      email: [''],
      phoneNumber: [''],
      address: [''],
      school: [''],
    });

    this._activeRoute.paramMap.subscribe(params => {
      const idString = params.get('id');
      if (idString) {
        this.parentID = parseInt(idString);
      }
    })
    this.loadData();

  }

  loadData() {
    this._parentService.getParentDetailById(this.parentID).subscribe({
      next: response => {
        this.parent = response;
        this.school = response.school;
        console.log(this.school)
        this.parentForm.setValue({
          fullName: this.parent.fullName,
          dob: this.parent.dob,
          email: this.parent.account.email,
          phoneNumber: this.parent.phoneNumber,
          address: ((this.parent?.street ? this.parent.street + ", " : "")
            + (this.parent?.ward ? this.parent.ward.name + ", " : "")
            + (this.parent?.district ? this.parent.district.name + ", " : "")
            + (this.parent?.city ? this.parent.city.name : "")),
          school: Number(this.schoolId) || '',
        });
      }, error: error => {
        this.loadingError = true; // Đánh dấu có lỗi xảy ra
          // this._router.navigate(['/admin/parent']);
      }
    });
  }

  unEnrollNotification(type: string): void {
    this.notification.create(
      type,
      'Update success!!',
      'Parent has been unenrolled'
    );
  }

  enrollNotification(type: string): void {
    this.notification.create(
      type,
      'Update success!!',
      'Parent has been enrolled'
    );
  }

  errorNotification(type: string): void {
    this.notification.create(
      type,
      'Enroll failed!!',
      'Please try again'
    );
  }

  // getValue() {
  //   const school = this.parentForm.controls['school'].value;
  //   if (school) {
  //     this.schoolID = school.id;
  //   }
  // }

  getText() {
    return this.school.length !== 0 ? 'Select a school' : 'Not have school yet';
  }

  enroll() {
    this._parentService.enrollParent(this.parentID, this.schoolId).subscribe({
      next: res => {
        this._router.navigateByUrl('/admin/parent').then(r => {
          this.enrollNotification('success')
        });
      },
      error: err => {
        this.errorNotification('error');
      }
    });
  }

  ngAfterViewInit(): void {
    if (this.schoolId) {
      this.parentForm.get('school')?.setValue(this.schoolId);
    }
  }
}
