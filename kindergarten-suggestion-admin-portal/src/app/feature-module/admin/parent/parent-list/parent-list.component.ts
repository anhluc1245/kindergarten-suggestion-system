import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject, takeUntil} from "rxjs";
import {ActivatedRoute, NavigationExtras, Router} from "@angular/router";
import {ParentService} from "../../../../service/parent-service/parent-service.service";

@Component({
  selector: 'app-parent-list',
  templateUrl: './parent-list.component.html',
  styleUrls: ['./parent-list.component.scss']
})
export class ParentListComponent implements OnInit, OnDestroy {
  private unsubscribe$: Subject<void> = new Subject<void>();
  userList: any[] = [];
  searchOptions: string = '';
  dataLoaded = false;
  page = 0;
  size = 10;
  totalElements = 0;

  constructor(private _parentService: ParentService,
              private _activeRoute: ActivatedRoute,
              private _router: Router) {
  }

  ngOnInit(): void {
    this._activeRoute.queryParams.subscribe(params => {
      this.page = Number(params['page']) || 0;
      this.searchOptions = params['query'];
    });
    this.loadData();
  }

  search() {
    this.reGenerateUrl();
    this.loadData();
  }

  loadData() {
    this._parentService.getParentList(this.page, this.size, this.searchOptions)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: res => {
          this.userList = res.content;
          console.log(this.userList);
          this.totalElements = res.totalElements;
          this.dataLoaded = true;
        },
        error: error => {

        }
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
  paging(newPage: number) {
    this.page = newPage - 1;
    this.reGenerateUrl();
    this.loadData();
  }

  reGenerateUrl() {
    const navigationExtras: NavigationExtras = {
      queryParams: {query: this.searchOptions, page: this.page, size: this.size}
    };
    this._router.navigate([], navigationExtras);
  }
}
