import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SchoolListEnrollComponent } from './school-list-enroll.component';

describe('SchoolListEnrollComponent', () => {
  let component: SchoolListEnrollComponent;
  let fixture: ComponentFixture<SchoolListEnrollComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SchoolListEnrollComponent]
    });
    fixture = TestBed.createComponent(SchoolListEnrollComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
