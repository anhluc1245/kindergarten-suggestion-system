import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', pathMatch: "full", redirectTo: 'school' },
  { path: 'school', loadChildren: () => import('./school/school.module').then(m => m.SchoolModule) },
  { path: 'user', loadChildren: () => import('./user/user.module').then(m => m.UserModule) },
  { path: 'parent', loadChildren: () => import('./parent/parent.module').then(m => m.ParentModule) },
  { path: 'request', loadChildren: () => import('./requests-counseling/requests-counseling.module').then(m => m.RequestsCounselingModule) },
  { path: 'reminder', loadChildren: () => import('./reminder/reminder.module').then(m => m.ReminderModule) }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {
}
