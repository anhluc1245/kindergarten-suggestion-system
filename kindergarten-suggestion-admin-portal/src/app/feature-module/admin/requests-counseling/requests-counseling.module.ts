import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzBreadCrumbModule } from 'ng-zorro-antd/breadcrumb';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzMentionModule } from 'ng-zorro-antd/mention';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { DobDatePipe } from 'src/app/pipe/dob-date.pipe';
import { RolePipe } from 'src/app/pipe/role.pipe';
import { DetailRequestComponent } from './detail-request/detail-request.component';
import { ListRequestComponent } from './list-request/list-request.component';
import { RequestsCounselingRoutingModule } from './requests-counseling-routing.module';
import { RequestStatusPipe } from "../../../pipe/request-status.pipe";
import { NzPaginationModule } from "ng-zorro-antd/pagination";


@NgModule({
  declarations: [
    ListRequestComponent,
    DetailRequestComponent,
  ],
  imports: [
    CommonModule,
    RequestsCounselingRoutingModule,
    NzTagModule,
    NzTableModule,
    NzPopconfirmModule,
    NzButtonModule,
    NzMentionModule,
    FormsModule,
    NzInputModule,
    NzIconModule,
    NzBreadCrumbModule,
    RolePipe,
    DobDatePipe,
    ReactiveFormsModule,
    FormsModule,
    NzSelectModule,
    NzCardModule,
    NzDividerModule,
    NzFormModule,
    NzNotificationModule,
    RequestStatusPipe,
    NzPaginationModule,
  ]
})
export class RequestsCounselingModule { }
