import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailRequestComponent } from './detail-request/detail-request.component';
import { ListRequestComponent } from './list-request/list-request.component';

const routes: Routes = [
  { path: '', component: ListRequestComponent },
  { path: 'list', component: ListRequestComponent },
  { path: 'detail/:id', component: DetailRequestComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequestsCounselingRoutingModule { }
