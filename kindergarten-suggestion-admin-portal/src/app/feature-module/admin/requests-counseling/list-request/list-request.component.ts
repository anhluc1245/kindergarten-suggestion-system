import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from "@angular/router";
import { NzNotificationService } from "ng-zorro-antd/notification";
import { Subject, takeUntil } from "rxjs";
import { RequestService } from 'src/app/service/request-service/request.service';




@Component({
  selector: 'app-list-request',
  templateUrl: './list-request.component.html',
  styleUrls: ['./list-request.component.scss']
})
export class ListRequestComponent implements OnInit ,OnDestroy{
  private unsubscribe$: Subject<void> = new Subject<void>();
  requestList : any[] = [];
  searchOptions: string = '';
  dataLoaded = false;
  page = 0;
  size = 10;
  totalElements = 0;



  constructor(private _requestService: RequestService,
    private _router: Router,
    private _notification: NzNotificationService,
    private _activeRoute: ActivatedRoute,) {
}
 
  ngOnInit(): void {
    this._activeRoute.queryParams.subscribe(params => {
      this.page = Number(params['page']) || 0;
      this.searchOptions = params['query'];
    });

    const role = localStorage.getItem('role');
    this.loadData();
  }
  search() {
    this.reGenerateUrl();
    this.loadData();
  }
  loadData() {
    this._requestService.getRequestList(this.page,this.size,this.searchOptions).pipe(takeUntil(this.unsubscribe$)).subscribe({
      next: res => {
        this.requestList = res.content;
        this.dataLoaded = true;
        this.totalElements = res.totalElements;
      },
      error: error => {
        console.log("err");

      }
    });
  }
  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
  paging(newPage: number) {
    this.page = newPage - 1;
    this.reGenerateUrl();
    this.loadData();
  }

  reGenerateUrl() {
    const navigationExtras: NavigationExtras = {
      queryParams: {query: this.searchOptions, page: this.page, size: this.size}
    };
    this._router.navigate([], navigationExtras);
  }

}
 


