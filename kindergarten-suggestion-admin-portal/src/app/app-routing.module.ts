import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { authGuard } from "./guard/auth.guard";
import { EmptyLayoutComponent } from './layout/empty-layout/empty-layout.component';
import { ErrorAuthorizedComponent } from "./layout/error-layout/error-authorized/error-authorized.component";
import { ErrorForbidenComponent } from "./layout/error-layout/error-forbiden/error-forbiden.component";
import { ErrorNotfoundComponent } from "./layout/error-layout/error-notfound/error-notfound.component";
import { ErrorServerComponent } from "./layout/error-layout/error-server/error-server.component";
import { PrivateLayoutComponent } from './layout/private-layout/private-layout.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'admin'},
  {
    path: 'admin', component: PrivateLayoutComponent,
    canActivate : [authGuard],
    children: [
      {
        path: '',
        loadChildren: () => import('./feature-module/admin/admin.module').then(m => m.AdminModule)}

    ]
  },
  {
    path: 'admin/auth',
    component: EmptyLayoutComponent,
    loadChildren: () => import('./feature-module/auth/auth.module').then(m => m.AuthModule)
  },
  {
    path: 'error/403',
    component: ErrorForbidenComponent,
  },  {
    path: 'error/404',
    component: ErrorNotfoundComponent,
  },  {
    path: 'error/401',
    component: ErrorAuthorizedComponent,
  },  {
    path: 'error/500',
    component: ErrorServerComponent,
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
