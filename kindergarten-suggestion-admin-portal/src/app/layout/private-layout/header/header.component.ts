import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { AdminServiceService } from 'src/app/service/admin-service/admin-service.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  confirmModal?: NzModalRef; // For testing by now
  token: any;
  userName!: string;
  constructor(private modal: NzModalService,
    private _adminService: AdminServiceService,
    private _router: Router) { }
  ngOnInit(): void {
    this.token = localStorage.getItem('accessToken');
    // this.userName = localStorage.getItem('userName') || '';
    this.userName = this.createUserName(localStorage.getItem('userName') || '');;
  }

  showConfirm(): void {
    this.confirmModal = this.modal.confirm({
      nzTitle: 'Are you leaving?',
      nzContent: 'Are you sure you want to logout? \n'+
                  'All you unsaved data will be lost',
      nzOkText: 'Yes',
      nzOkType: 'primary',
      nzOnOk: () =>
        this._adminService.logOut().subscribe({
          next: (resp) => {
            localStorage.clear();
            this._router.navigate(['/admin/auth/login']).then(r => window.location.reload());
          },
          error : (error) => {
              // Xử lý lỗi logout, ví dụ: hiển thị thông báo lỗi.
              console.error('Logout failed:', error);
            }
        }),
      nzCancelText: 'No, please take me back',
      nzOnCancel: () => console.log('Cancel')
    });
  }

  createUserName(userName: string) {
    const arrStr = userName.split(' ');
    let newName = '';
    arrStr.forEach(str => {
      newName += str.charAt(0).toUpperCase();
    })
    return newName;
  }
}
