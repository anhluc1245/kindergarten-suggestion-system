import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'requestStatus',
  standalone: true
})
export class RequestStatusPipe implements PipeTransform {

  transform(inputString: string): string {
    if (inputString.length === 0) {
      return inputString; // Xử lý trường hợp chuỗi rỗng (empty string)
    }



    const firstLetter = inputString.charAt(0).toUpperCase();
    const restOfString = inputString.slice(1).toLowerCase(); // Lấy phần còn lại của chuỗi
    return firstLetter + restOfString;
  }
}
