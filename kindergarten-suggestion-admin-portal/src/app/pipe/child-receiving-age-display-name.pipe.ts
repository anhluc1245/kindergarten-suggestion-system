import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'displayName',
  standalone: true
})
export class ChildReceivingAgeDisplayNamePipe implements PipeTransform {
  transform(value: string): string {
    // Chuyển đổi giá trị từ enum thành displayName
    switch (value) {
      case 'AGE_6M_1Y':
        return "From 6 months to 1 year";
      case 'AGE_1Y_3Y':
        return "From 1 year to 3 years";
      case 'AGE_3Y_6Y':
        return "From 3 year to 6 years";
      default:
        return value;

    }
  }
}
