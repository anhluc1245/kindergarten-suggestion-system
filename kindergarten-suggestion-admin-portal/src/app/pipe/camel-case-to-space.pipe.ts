import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'camelCaseToSpace'
})
export class CamelCaseToSpacePipe implements PipeTransform {

  transform(value: string): string {
    // Tách các từ từ chuỗi LearningProgramRating ->  Learing Program Rating
    const words = value.split(/(?=[A-Z])/);

    // Thay thế các chuỗi "And" thành dấu "&"
    const formattedWords = words.map(word => {
      return word.charAt(0).toUpperCase() + word.slice(1);
    });

    return formattedWords.join(' ');
  }
}
