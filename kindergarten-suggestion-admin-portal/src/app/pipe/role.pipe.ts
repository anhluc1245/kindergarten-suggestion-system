import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'role',
  standalone: true
})
export class RolePipe implements PipeTransform {

  transform(value: string): string {
    if (!value) {
      return '';
    }
    if (value === 'SCHOOL_OWNER') {
      return 'School Owner';
    }
    return value
      .toLowerCase()
      .replace(/_/g, ' ')
      .replace(/\b\w/g, firstChar => firstChar.toLocaleUpperCase());
  }

}
