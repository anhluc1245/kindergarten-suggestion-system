import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../environment/environment";

@Injectable({
  providedIn: 'root'
})
export class SchoolService {

  constructor(private _httpClient: HttpClient) {
  }

  getSchoolTypes(): Observable<any> {
    return this._httpClient.get(`${environment.apiPrefixUrl}/enum/SchoolTypes`);
  }

  getAdmissionAge(): Observable<any> {
    return this._httpClient.get(`${environment.apiPrefixUrl}/enum/AdmissionAge`);
  }

  getEducationMethod(): Observable<any> {
    return this._httpClient.get(`${environment.apiPrefixUrl}/enum/EducationMethod`);
  }

  getFacility(): Observable<any> {
    return this._httpClient.get(`${environment.apiPrefixUrl}/facilities`);
  }

  getUtility(): Observable<any> {
    return this._httpClient.get(`${environment.apiPrefixUrl}/utilities`);
  }

  getAllProvince(): Observable<any> {
    return this._httpClient.get(`http://localhost:8686/api/local/pro`);
  }

  getAllDistrictByProvinceID(id: number): Observable<any> {
    return this._httpClient.get(`http://localhost:8686/api/local/dis/${id}`);
  }

  getAllWardByDistrictID(id: number): Observable<any> {
    return this._httpClient.get(`http://localhost:8686/api/local/war/${id}`);
  }

  getAllRatingsSchool(id: number): Observable<any> {
    return this._httpClient.get(`${environment.apiPrefixUrl}/school/${id}/average-feedback`);
  }


  getAllFeedBacksSchool(id: number, page: number, searchOptions: any): Observable<any> {
    let params = new HttpParams()
      .set('page', page.toString());
    if (searchOptions) {
      for (const key in searchOptions) {
        if (searchOptions.hasOwnProperty(key)) {
          const value = searchOptions[key];
          if (value !== undefined && value !== null && value !== '') {
            // Xử lý các trường hợp khác
            params = params.set(key, searchOptions[key]);
          }
        }
      }
    }
    return this._httpClient.get(`${environment.apiPrefixUrl}/school/${id}/feedbacks`, {
      params
    });
  }


  // getAllFeedBacksSchool(id: number, page: number, start: string, end: string): Observable<any> {
  //   return this._httpClient.get(`${environment.apiPrefixUrl}/school/${id}/feedbacks`, {
  //     params: {
  //       page, start, end
  //     }
  //   });
  // }
}
