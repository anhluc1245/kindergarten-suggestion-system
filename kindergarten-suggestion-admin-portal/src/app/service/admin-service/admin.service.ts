import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { environment } from 'src/app/environment/environment';


@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private _httpClient: HttpClient) { }

  createUserByAdmin(payload: any): Observable<any> {
    return this._httpClient.post(`${environment.apiPrefixUrl}/admin/user/create`, payload);
  }

  updateUserByAdmin(id: number, payload: any): Observable<any> {
    return this._httpClient.put(`${environment.apiPrefixUrl}/admin/user/update/${id}`, payload, { responseType: 'text' });
  }

  getRoleUser(): Observable<any> {
    return this._httpClient.get(`${environment.apiPrefixUrl}/admin/user/role`);
  }
}
