import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../environment/environment";

@Injectable({
    providedIn: 'root'
})
export class AdminSchoolService {

    constructor(private _httpClient: HttpClient) {
    }

    getSchoolDetail(id: number): Observable<any> {
        return this._httpClient.get(`${environment.apiPrefixUrl}/owner/school/${id}`)
    }

    postSchool(formData: any, action: string): Observable<any> {
        return this._httpClient.post(`${environment.apiPrefixUrl}/owner/school/${action}`, formData);
    }

    putSchool(formData: any, action: string): Observable<any> {
        let role = 'owner';
        if (action == 'approve' || action == 'reject') {
            role = 'admin'
        }
        return this._httpClient.put(`${environment.apiPrefixUrl}/${role}/school/${action}`, formData);
    }

    patchSchool(action: string, id: number): Observable<any> {
        let role = 'owner';
        if (action == 'approve' || action == 'reject') {
            role = 'admin'
        }
        return this._httpClient.patch(`${environment.apiPrefixUrl}/${role}/school/${action}/${id}`, null);
    }

    deleteSchool(id: number): Observable<any> {
        return this._httpClient.delete(`${environment.apiPrefixUrl}/owner/school/${id}`)
    }

    getSchoolList(searchOptions: any, page : number, size: number): Observable<any> {
      let params = new HttpParams()
        .set('page', page)
        .set('size', size);

      if (searchOptions) {
        params = params.set('q', searchOptions);
      }
        return this._httpClient.get(`${environment.apiPrefixUrl}/owner/school/list`, {params});
    }
}
