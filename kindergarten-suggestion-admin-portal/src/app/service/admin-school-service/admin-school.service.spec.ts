import { TestBed } from '@angular/core/testing';

import { AdminSchoolService } from './admin-school.service';

describe('AdminSchoolServiceService', () => {
  let service: AdminSchoolService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AdminSchoolService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
