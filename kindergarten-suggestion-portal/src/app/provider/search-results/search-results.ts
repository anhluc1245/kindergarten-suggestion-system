import {EventEmitter, Injectable, Output} from '@angular/core';

@Injectable({
    providedIn: 'root',
})

export class SearchResults {
  searchedSchoolList: [] = [];
  totalElements: number = 0;
  totalPages: number = 0;
  activePage!: number;
}
