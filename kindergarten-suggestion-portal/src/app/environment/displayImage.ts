export class ImageUtility {
  static displayImg(img: string): string {
    if (!img) {
      return 'https://m.media-amazon.com/images/M/MV5BYzFlMjgwOTgtMjVlNy00NzliLTgxYzgtYTlmMmMzZTFiMTc2XkEyXkFqcGdeQXVyODg3NDc1OTE@._V1_.jpg';
    }
    if (img.startsWith('http')) {
      return img;
    }
    img = img.replace("\\", "/");
    return `http://localhost:8686/public/files/${img}`;
  }
}
