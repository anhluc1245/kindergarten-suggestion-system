import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {environment} from "../../environment/environment";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class SchoolService {

  constructor(private _httpClient : HttpClient) { }

  getEnrolledSchool(page: number, size: number, status: boolean) : Observable<any> {
    return this._httpClient.get(`${environment.apiPrefixUrl}/school/enrolled`,
      {params : {page, size, status}})
  }
  getDetailSchool(schoolId:number):Observable<any>{
    return this._httpClient.get(`${environment.apiPrefixUrl}/school/${schoolId}`);
  }
  getSchoolList(page: number, size: number,searchOptions: any ): Observable<any> {
    let params = new HttpParams()
      .set('page', page.toString())
      .set('size', size.toString());

    if (searchOptions) {
      for (const key in searchOptions) {
        if (searchOptions.hasOwnProperty(key) && searchOptions[key]) {
          params = params.set(key, searchOptions[key]);
        }
      }
    }
    return this._httpClient.get(`${environment.apiPrefixUrl}/school/search`, {
      params
    });
  }


  getSchoolTypes(): Observable<any> {
    return this._httpClient.get(`${environment.apiPrefixUrl}/enum/SchoolTypes`, {
    });
  }
  getAdmissionAge(): Observable<any> {
    return this._httpClient.get(`${environment.apiPrefixUrl}/enum/AdmissionAge`, {
    });
  }
  getTopNFeedBack():Observable<any>{
    return this._httpClient.get(`${environment.apiPrefixUrl}/school`);
  }
  getListFacility():Observable<any>{
    return this._httpClient.get(`${environment.apiPrefixUrl}/enum/Facility`);
  }
  getListUtility():Observable<any>{
    return this._httpClient.get(`${environment.apiPrefixUrl}/enum/UtilityEnum`);
  }
}
