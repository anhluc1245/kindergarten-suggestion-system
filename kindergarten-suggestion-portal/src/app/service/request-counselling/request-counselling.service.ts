import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/app/environment/environment';

@Injectable({
  providedIn: 'root'
})
export class RequestCounsellingService {

  constructor(private _httpClient: HttpClient) { }

  createRequest(formData: any): Observable<any> {
    return this._httpClient.post(`${environment.apiPrefixUrl}/request_counselling`, formData)
  }

  getRequestById(page: number, size: number): Observable<any> {
    return this._httpClient.get(`${environment.apiPrefixUrl}/request_counselling`, {
      params: { page: page, size: size }
    });
  }

  cancelRequest(id: number): Observable<any> {
    return this._httpClient.put(`${environment.apiPrefixUrl}/request_counselling/cancel/${id}`, id, { responseType: 'text' });
  }

}
