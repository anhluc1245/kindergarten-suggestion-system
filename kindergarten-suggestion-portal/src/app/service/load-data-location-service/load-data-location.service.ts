import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../environment/environment";

@Injectable({
  providedIn: 'root'
})
export class LoadDataLocationService {

  constructor(private _http: HttpClient) {
  }

  getAllProvince(): Observable<any> {
    return this._http.get(`${environment.apiPrefixUrl}/local/pro`);
  }
  getAllDistrictByProvinceID(id:number): Observable<any> {
    return this._http.get(`${environment.apiPrefixUrl}/local/dis/${id}`);
  }
  getAllWardByDistrictID(id:number): Observable<any> {
    return this._http.get(`${environment.apiPrefixUrl}/local/war/${id}`);
  }
}
