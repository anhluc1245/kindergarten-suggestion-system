import { TestBed } from '@angular/core/testing';

import { LoadDataLocationService } from './load-data-location.service';

describe('LoadDataLocationService', () => {
  let service: LoadDataLocationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LoadDataLocationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
