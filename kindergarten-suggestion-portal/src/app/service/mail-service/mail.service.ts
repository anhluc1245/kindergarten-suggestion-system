import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/app/environment/environment';

@Injectable({
  providedIn: 'root'
})
export class MailService {

  constructor(private _httpClient: HttpClient) { }
  confirmEmail(authPayload: any): Observable<any>{
    return this._httpClient.post(`${environment.apiPrefixUrl}/mail/register`, authPayload)
  }
  forgotPassword(authPayload: any): Observable<any> {
    return this._httpClient.post(`${environment.apiPrefixUrl}/mail/password`, authPayload);
  }
  requestCounsellingEmail(authPayload: any): Observable<any>{
    return this._httpClient.post(`${environment.apiPrefixUrl}/mail/request`, authPayload);
  }
}
