import { TestBed } from '@angular/core/testing';

import { SearchComunicationService } from './search-comunication.service';

describe('SearchComunicationService', () => {
  let service: SearchComunicationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SearchComunicationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
