import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {environment} from "../environment/environment";

@Injectable({
  providedIn: 'root'
})
export class SchoolDetailService {

  //http://localhost:8686/api/school/id (link http call API)
  constructor(private _httpClient: HttpClient) {
  }

  getDetailSchool(schoolId: number, userId: any): Observable<any> {
    const params: { [key: string]: any } = {};
    if (userId) {
      params['userId'] = userId;
    }
    return this._httpClient.get(`${environment.apiPrefixUrl}/school/${schoolId}`, { params });
  }
  getListFacility():Observable<any>{
    return this._httpClient.get(`${environment.apiPrefixUrl}/enum/Facility`);
  }
  getListUtility():Observable<any>{
    return this._httpClient.get(`${environment.apiPrefixUrl}/enum/UtilityEnum`);
  }
}
