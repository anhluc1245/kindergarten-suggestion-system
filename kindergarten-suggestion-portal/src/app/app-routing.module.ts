import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PublicLayoutComponent } from './layout/public-layout/public-layout.component';
import {ErrorForbidenComponent} from "./layout/error-layout/error-forbiden/error-forbiden.component";
import {ErrorNotfoundComponent} from "./layout/error-layout/error-notfound/error-notfound.component";
import {ErrorAuthorizedComponent} from "./layout/error-layout/error-authorized/error-authorized.component";
import {ErrorServerComponent} from "./layout/error-layout/error-server/error-server.component";
const routes: Routes = [
  {
    path: '', component: PublicLayoutComponent,
    children: [
      {path: '', pathMatch: 'full', redirectTo: 'home'},
      {path: 'home', loadChildren: () => import('./feature-module/public/school/school.module').then(m => m.SchoolModule)},
      {path: 'account', loadChildren: () => import('./feature-module/public/account/account.module').then(m => m.AccountModule) },
      {path: 'auth', loadChildren: () => import('./feature-module/auth/auth.module').then(m => m.AuthModule)}
    ]
  },
  {
    path: 'error/403',
    component: ErrorForbidenComponent,
  },  {
    path: 'error/404',
    component: ErrorNotfoundComponent,
  },  {
    path: 'error/401',
    component: ErrorAuthorizedComponent,
  },  {
    path: 'error/500',
    component: ErrorServerComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
