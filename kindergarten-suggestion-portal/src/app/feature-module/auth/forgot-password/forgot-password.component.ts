import { CommonModule } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MailService } from 'src/app/service/mail-service/mail.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
  standalone: true,
  imports: [
    ReactiveFormsModule,
    CommonModule
  ],
})
export class ForgotPasswordComponent implements OnInit {
  @ViewChild('closeButton')
  closeButton!: ElementRef<HTMLElement>;

  errorMessage!: string;
  emailForm!:FormGroup;
  inputEmailTouched = false;

  constructor(private _formBuilder: FormBuilder,
    private _mailService: MailService,
    private _toastr: ToastrService,
    private _router: Router
 ){}

  ngOnInit(): void {
    this.emailForm = this._formBuilder.group({
      email: ['', [Validators.required, Validators.pattern(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)]]
      
    });
    // Theo dõi sự kiện focus của ô input
    this.email?.valueChanges.subscribe(() => {
      this.inputEmailTouched = true;
    });
  }
  onSubmit(){
    this.inputEmailTouched = true;
    this.errorMessage="";
    if(this.emailForm.invalid){
      return;
    }
    console.log(this.emailForm.value);
    this._mailService.forgotPassword(this.emailForm.value).subscribe({
      next: (response:any) => {
        this.showSuccess();
        setTimeout(() => this.closeModal(),2000);
        
      }, error: (error:any) =>{
        this.errorMessage = "Email is invalid!"
        this.showFail();
      }
    });
  
  }
  closeModal(){
    let el: HTMLElement = this.closeButton.nativeElement;
        el.click();
  }
  showSuccess() {
    this._toastr.success('We have sent an email with the link to reset your password!');
  }
  showFail() {
    this._toastr.error('The email address doesn’t exist. Please try again!');}
get email() { return this.emailForm.get('email'); }
}