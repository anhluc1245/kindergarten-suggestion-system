import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/service/auth.service';
import { MailService } from 'src/app/service/mail-service/mail.service';

@Component({
  selector: 'app-account-active',
  templateUrl: './account-active.component.html',
  styleUrls: ['./account-active.component.scss']
})
export class AccountActiveComponent implements OnInit {
  emailForm!: FormGroup;
  errorMessage!: string;
  tokenAccess!: string;
  isDoneVisible: boolean = false;
  isErrorVisible: boolean = false;
  inputEmailTouched=false;
  constructor(private _authServie:AuthService,
              private _router: Router,
              private _mailService: MailService,
              private _toastr: ToastrService,
              private _activatedRoute: ActivatedRoute,
              private _formBuilder: FormBuilder){}
  ngOnInit(): void {
    this.tokenAccess = this._activatedRoute.snapshot.params?.['token'];
    if (this.tokenAccess) {
    console.log(this.tokenAccess);
    this.callAccountActive()
    .then((response) => {
      this.isDoneVisible = true;
      setTimeout(() => this.homePage(),5000);
    })
    .catch((err) => {
      console.log(err.status);
      this.isErrorVisible = true;
      this.emailForm = this._formBuilder.group({
        email: ['',[Validators.required, Validators.pattern(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)]],
      });
    });
    }
  }
  async callAccountActive(): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this._authServie.accountAcctive(this.tokenAccess).subscribe({
        next: (response) => {
          resolve(response); // Giải quyết Promise khi API gọi thành công
        },
        error: (err) => {
          console.log(err);
          reject(err); // Bắt Promise khi xảy ra lỗi
        }
      });
    });
  }
  submitForm(): void {
    this.inputEmailTouched = true;
    this.errorMessage="";
    if(this.emailForm.invalid){
      return;
    }
    this._mailService.confirmEmail(this.emailForm.value).subscribe({
      next: (response)=>{
        this.showSuccess();
        
      },error:(err)=> {
        this.showFail();
      },
    })
  }
  homePage(): void {
    this._router.navigate(['/home']);
  }
  showSuccess() {
    this._toastr.success('The confirmation code has been sent to your email, please check your email again!');
  }
  showFail() {
    this._toastr.error('Sending confirmation code failed, the account may have been successfully activated or not exist!');}
  get email() { return this.emailForm.get('email'); }
}
