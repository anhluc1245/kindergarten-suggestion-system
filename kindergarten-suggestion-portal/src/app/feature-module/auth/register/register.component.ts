import { CommonModule } from '@angular/common';
import "@angular/compiler";
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import { MailService } from 'src/app/service/mail-service/mail.service';
import { AuthService } from "../../../service/auth.service";

@Component({
  selector: 'app-register',
  standalone: true,
  templateUrl: './register.component.html',
  imports: [
    ReactiveFormsModule,
    CommonModule
  ],
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm!: FormGroup;
  mailForm: any;
  errorMessage!: string;
  successMessage!: string;
  @ViewChild('submit') submitBtn!: ElementRef;
  @ViewChild('cancel') cancelBtn!: ElementRef;
  passwordsMatchChecked: boolean = false;


  constructor(private _formBuilder: FormBuilder,
              private toastr: ToastrService,
              private _authService: AuthService,
              private _mailService: MailService) { }

  ngOnInit(): void {
    this.registerForm = this._formBuilder.group({
      fullName: ['', Validators.required],
      email: ['', {
        validators: [Validators.required, Validators.pattern(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)],
        updateOn: 'blur' // This will trigger validation on blur
      }],
      phoneNumber: ['', {
        validators: [Validators.required, Validators.pattern(/^(0[35789][0-9]{8}|02[0-9]{9})$/)],
        updateOn: 'blur' // This will trigger validation on blur
      }],
      password: ['', {
        validators: [Validators.required, Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d).{7,}$/)],
        updateOn: 'blur' // This will trigger validation on blur
      }],
      confirmPassword: ['', Validators.required]
    }, {
      validator: this.passwordMatchValidator,
    });
  }


  passwordMatchValidator(formGroup: FormGroup) {

    const password = formGroup.get('password')?.value;
    const confirmPassword = formGroup.get('confirmPassword')?.value;

    if (password !== confirmPassword) {
      formGroup.get('confirmPassword')?.setErrors({ mismatch: true });
    } else {
      formGroup.get('confirmPassword')?.setErrors(null);
    }
  }


  onSubmit() {
    this.errorMessage = '';
    this.successMessage = '';
    this.passwordsMatchChecked = true;
    if (this.registerForm.invalid) {
      return;
    }
    this.mailForm = {
      email:this.registerForm.value.email,
      fullName:this.registerForm.value.fullName
    }
    this._authService.register(this.registerForm.value).subscribe({
      next: res => {
            this.successMessage = '';
            this.showSuccess()
            this.callConfirmEmail(this.mailForm);// Gọi API confirmEmail sau khi đăng ký thành công
      },
      error: error => {
        if (error.status === 400) { // Kiểm tra mã trạng thái 409 (CONFLICT)
          this.errorMessage = error.error; // Gán thông báo từ phía backend
        } else {
          this.errorMessage = 'An error occurred while registering an account.';
        }
      }
    })
  }
  callConfirmEmail(data:any) {
    this._mailService.confirmEmail(data).subscribe({
      next: response => {
        // Xử lý thành công khi gọi API confirmEmail
        this.showSendEmailSuccess();
      },
      error: error => {
        // Xử lý lỗi khi gọi API confirmEmail
        this.showSendEmailFail();
      }
    });
  }


    showSuccess() {
        this.toastr.success('Register successfully! ');
        this.registerForm.reset();
        this.cancelBtn.nativeElement.click();
        this.submitBtn.nativeElement.setAttribute('data-bs-target', '#loginModal');
        this.submitBtn.nativeElement.setAttribute('data-bs-toggle', 'modal');
        this.submitBtn.nativeElement.click();
        this.submitBtn.nativeElement.removeAttribute('data-bs-target');
        this.submitBtn.nativeElement.removeAttribute('data-bs-toggle');
    }
    showSendEmailSuccess() {
      this.toastr.success('Please check your email to active account!');
    }
    showSendEmailFail() {
      this.toastr.error('Can not send confirmCode to your email! Please check again!');
    }
  clearForm() {
    this.registerForm.reset();
    this.errorMessage = '';
  }

  get fullName() { return this.registerForm.get('fullName'); }
  get phoneNumber() { return this.registerForm.get('phoneNumber'); }
  get email() { return this.registerForm.get('email'); }
  get password() { return this.registerForm.get('password'); }
  get confirmPassword() { return this.registerForm.get('confirmPassword'); }

}
