import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  resetPasswordForm!: FormGroup;
  tokenAccess!: string;
  passwordTouched = false;
  confirmPasswordTouched = false;
  passwordsMatchChecked: boolean = false;

  constructor(private _formBuilder:FormBuilder,
              private _router: Router,
              private _activatedRoute: ActivatedRoute,
              private _toastr: ToastrService,
              private _userService: UserService){}
  ngOnInit(): void {
    this.tokenAccess = this._activatedRoute.snapshot.params?.['token'];
    this.loadData();
    this.resetPasswordForm = this._formBuilder.group({
      password: ['', [Validators.required, Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d).{7,}$/)]],
      confirmPassword: ['', [Validators.required]],
      token:this.tokenAccess
    }, {
      validator: this.passwordMatchValidator,
    });
    // Theo dõi sự kiện focus của ô input
    this.confirmPassword?.valueChanges.subscribe(() => {
      this.confirmPasswordTouched = true;
    });
    this.password?.valueChanges.subscribe(() => {
      this.passwordTouched = true;
    });
    console.log(this.confirmPassword?.errors?.['required']);
  }
  loadData(): void {
    this._userService.loadResetPage(this.tokenAccess).subscribe({
      next: response =>{
        //FIXME
        console.log("OK");
      },
      error: err => {
        this._router.navigateByUrl('/error/404');
      }
    })
  }
  passwordMatchValidator(formGroup: FormGroup) {

    const password = formGroup.get('password')?.value;
    const confirmPassword = formGroup.get('confirmPassword')?.value;

    if (password !== confirmPassword) {
      formGroup.get('confirmPassword')?.setErrors({ mismatch: true });
    } else {
      formGroup.get('confirmPassword')?.setErrors(null);
    }
  }
  onSubmit() {
    console.log(this.confirmPassword?.errors?.['required']);
    this.passwordTouched = true;
    this.confirmPasswordTouched = true;
    this.passwordsMatchChecked = true;
    if(this.resetPasswordForm.invalid){
      return;
    }
    console.log(this.resetPasswordForm.value);
    this._userService.resetPassword(this.resetPasswordForm.value).subscribe({
      next: response => {
        this.showSuccess();
        setTimeout(() => this.homePage(),2000);
      },
      error: error => {
        this.showFail();
      }
    });
  }
  homePage(): void {
    this._router.navigate(['/home']);
  }
  showSuccess() {
    this._toastr.success('Email has been reset password, you can log in to your account!');
  }
  showFail() {
    this._toastr.error('This link has expired. Please go back to Homepage and try again!');}
  get password() { return this.resetPasswordForm.get('password'); }
  get confirmPassword() { return this.resetPasswordForm.get('confirmPassword'); }
}
