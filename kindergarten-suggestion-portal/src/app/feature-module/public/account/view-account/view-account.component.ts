import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {UserService} from "../../../../service/user.service";
import {finalize, Subject, takeUntil} from "rxjs";
import {LoadDataLocationService} from "../../../../service/load-data-location.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-view-account',
  templateUrl: './view-account.component.html',
  styleUrls: ['./view-account.component.scss']
})
export class ViewAccountComponent implements OnInit, OnDestroy {
  private unsubscribe$: Subject<void> = new Subject<void>();
  userForm!: FormGroup;
  isSubmittingForm = false;
  errorMessage!: string;
  cities: any[] = [];
  districts: any[] = [];
  wards: any[] = [];

  constructor(private _formBuilder: FormBuilder,
              private _userService: UserService,
              private _locationService: LoadDataLocationService,
              private _toastr: ToastrService,
              private _router: Router
  ) {
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  ngOnInit(): void {
    this.userForm = this._formBuilder.group({
      id: [''],
      fullName: ['', Validators.required],
      dob: ['', Validators.required],
      email: ['', Validators.required],
      phoneNumber: ['', [Validators.required, Validators.pattern(/^(0[35789][0-9]{8}|02[0-9]{9})$/)]],
      city: ['', Validators.required],
      district: ['', Validators.required],
      ward: ['', Validators.required],
      street: ['', Validators.required],
    });

    this.setData();

    this.userForm.controls['city'].valueChanges.pipe(takeUntil(this.unsubscribe$)).subscribe((value) => {
      if (value) {
        this.fetchDistrictListByProvinceID(value);
        this.userForm.get('district')?.patchValue('');
        this.userForm.get('ward')?.setValue('');
      }
    })

    this.userForm.controls['district'].valueChanges.pipe(takeUntil(this.unsubscribe$)).subscribe((value) => {
      if (value) {
        this.fetchWardListByDistrictID(value);
        this.userForm.get('ward')?.setValue('');
      }
    })
  }

  setData() {
    this._userService.getUser().pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: res => {
          const userData = res;
          console.log(userData);
          if (userData) {
            this.userForm.setValue({
              id: userData.id,
              fullName: userData.fullName,
              dob: userData.dob ? this.formatDate(userData.dob) : '',
              email: userData.account.email,
              phoneNumber: userData.phoneNumber,
              city: userData.city?.id || '',
              district: userData.district?.id || '',
              ward: userData.ward?.id || '',
              street: userData.street || '',
            })
          }
        },
        error: err => {
        }
      });
    this.fetchListCity();
    this.fetchDistrictListByProvinceID(this.userForm.get('city')?.value);
    this.fetchWardListByDistrictID(this.userForm.get('district')?.value);
  }

  fetchListCity() {
    this._locationService.getAllProvince().pipe(takeUntil(this.unsubscribe$)).subscribe({
      next: resp => {
        this.cities = resp;
      },
      error: err => {
        this.cities = [];
      }
    });
  }

  fetchDistrictListByProvinceID(provinceID: number) {
    if (provinceID) {
      this._locationService.getAllDistrictByProvinceID(provinceID).pipe(takeUntil(this.unsubscribe$)).subscribe(district => {
        this.districts = district;
      });
    }
  }

  fetchWardListByDistrictID(districtID: number) {
    if (districtID) {
      this._locationService.getAllWardByDistrictID(districtID).pipe(takeUntil(this.unsubscribe$)).subscribe(ward => {
        this.wards = ward;
      });
    }
  }

  submitForm() {
    if (this.userForm.invalid) {
      return;
    }

    this.isSubmittingForm = true;
    const userFormPayload = this.userForm.value;
    this._userService.update(userFormPayload)
      .pipe(finalize(() => this.isSubmittingForm = false), takeUntil(this.unsubscribe$))
      .subscribe({
        next: res => {
          this.showSuccess();
          this._router.navigate(['/account/profile']).then(() => window.scrollTo(0, 0));
        },
        error: err => {
          if (err.status === 409) {
            this.errorMessage = err.error.message;
          }
          this.showFail();
        }
      });
  }

  clearForm() {
    this.userForm.reset();
    this.setData();
  }

  showSuccess() {
    this._toastr.success('Update profile successfully!');
  }

  showFail() {
    this._toastr.error('Update profile failed!');
  }

  private formatDate(date: string) {
    let d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2)
      month = '0' + month;
    if (day.length < 2)
      day = '0' + day;

    return [year, month, day].join('-'); // yyyy-mm-dd
  }

  get dob() {
    return this.userForm.get('dOB');
  }

  get phoneNumber() {
    return this.userForm.get('phoneNumber');
  }
}
