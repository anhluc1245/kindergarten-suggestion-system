import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ReactiveFormsModule } from "@angular/forms";
import { NgxPaginationModule } from 'ngx-pagination';
import { SchoolModule } from '../school/school.module';
import { AccountRoutingModule } from './account-routing.module';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { MyRequestComponent } from './my-request/my-request.component';
import {ChildReceivingAgeDisplayNamePipe} from "../../../pipe/child-receiving-age-display-name.pipe";
import {DateFormatPipe} from "../../../pipe/date-format.pipe";

@NgModule({
    declarations: [
        ForgotPasswordComponent,
        MyRequestComponent,
    ],
    imports: [
        CommonModule,
        AccountRoutingModule,
        ReactiveFormsModule,
        SchoolModule,
        NgxPaginationModule,
        ChildReceivingAgeDisplayNamePipe,
        DateFormatPipe,
    ],
    exports: [
        ForgotPasswordComponent
    ]
})
export class AccountModule { }
