import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { finalize } from "rxjs";
import { AuthService } from 'src/app/service/auth.service';
import { UserService } from "../../../../service/user.service";

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  passwordForm!: FormGroup;
  isSubmittingForm = false;
  selectedId!: number;
  id: number | null = null;

  constructor(private _formBuilder: FormBuilder,
              private _router: Router,
              private _userService: UserService,
              private _toastr: ToastrService,
              private _authService: AuthService
  ) {
  }

  ngOnInit(): void {
    const userData = localStorage.getItem('userData');
    if (userData) {
      const parsedUserData = JSON.parse(userData);
      this.id = parsedUserData.userId;
    }
    this.passwordForm = this._formBuilder.group({
      id: [this.id],
      currentPassword: ['', Validators.required],
      newPassword: ['', [Validators.required, Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d).{7,}$/)]],
      confirmPassword: ['', Validators.required],
    },{
      validator: this.passwordMatchValidator
    });
  }
  passwordMatchValidator(formGroup: FormGroup) {
    const password = formGroup.get('newPassword')?.value;
    const confirmPassword = formGroup.get('confirmPassword')?.value;

    if (password !== confirmPassword) {
      formGroup.get('confirmPassword')?.setErrors({ mismatch: true });
    } else {
      formGroup.get('confirmPassword')?.setErrors(null);
    }
  }

  submitForm() {
    if (this.passwordForm.invalid) {
      return;
    }

    this.isSubmittingForm = true;
    const passwordFormPayload = this.passwordForm.value;
    if (this.selectedId) {

    } else {
      this._userService.updatePassword(passwordFormPayload)
        .pipe(finalize(() => this.isSubmittingForm = false))
        .subscribe({
          next: res => {
            this.showSuccess();
            this.resetForm();
            setTimeout(()=>{
              this.logOut();
            this._router.navigateByUrl("/home");

            },2000);
          },
          error: err => {
            this.showFail();
          }
        });
    }
  }

  resetForm() {
    this.passwordForm.reset({
      currentPassword: '',
      newPassword: '',
      confirmPassword: '',
    });
  }

  clearForm() {
    this.resetForm();
  }
  showSuccess() {
    this._toastr.success('Change password successfully! You must login again!');
  }

  showFail() {
    this._toastr.error('Change password failed!');
  }
  logOut(){
    console.log("logout");
    this._authService.logOut().subscribe(
      () => {
        localStorage.clear();
        window.location.reload();
      },
      (error) => {
          // Xử lý lỗi logout, ví dụ: hiển thị thông báo lỗi.
          console.error('Logout failed:', error);
        }
      
      );
  }
  get currentPassword() { return this.passwordForm.get('currentPassword'); }
  get newPassword() { return this.passwordForm.get('newPassword'); }
  get confirmPassword() { return this.passwordForm.get('confirmPassword'); }
  
}
