import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-rating-star',
  templateUrl: './rating-star.component.html',
  styleUrls: ['./rating-star.component.scss']
})
export class RatingStarComponent implements OnInit, OnChanges {
  @Input() rate : number = 5;
  @Input() maxRate : number = 5;
  @Input() sizeInput : number = 6;
  rateClass : any[] = [];

  ngOnInit(): void {
    this.rateClass = [];
    if (!this.sizeInput) {
      this.sizeInput = 6;
    }
    for (let i = 0; i < this.maxRate; i++) {
      if (this.rate > i && this.rate < i+1) {
        this.rateClass[i] = `fa-regular fa-star-half-stroke fs-${this.sizeInput}`;
      } else if (this.rate > i) {
        this.rateClass[i] = `fa fa-star fs-${this.sizeInput}`;
      } else if (this.rate <= i) {
        this.rateClass[i] = `fa-regular fa-star fs-${this.sizeInput}`;
      }
    }
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.ngOnInit();
  }
}
