import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestCounsellingComponent } from './request-counselling.component';

describe('RequestCounsellingComponent', () => {
  let component: RequestCounsellingComponent;
  let fixture: ComponentFixture<RequestCounsellingComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RequestCounsellingComponent]
    });
    fixture = TestBed.createComponent(RequestCounsellingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
