import { Component, ElementRef, Input, OnInit, Renderer2, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { MailService } from 'src/app/service/mail-service/mail.service';
import { RequestCounsellingService } from 'src/app/service/request-counselling/request-counselling.service';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-request-counselling',
  templateUrl: './request-counselling.component.html',
  styleUrls: ['./request-counselling.component.scss'],

})

export class RequestCounsellingComponent implements OnInit {

  @Input()
  schoolId: number | undefined;
  @ViewChild('close') closeModal!: ElementRef;
  @ViewChild('button') button!: ElementRef;
  validateForm!: FormGroup;
  accessToken: any = localStorage.getItem('accessToken');
  user: any = localStorage.getItem('userData');
  userData: any = JSON.parse(this.user);

  constructor(private _formBuilder: FormBuilder,
    private _requestService: RequestCounsellingService,
    private toastr: ToastrService,
    private _userService: UserService,
    private _mailService: MailService) { }

  ngOnInit(): void {
    this.validateForm = this._formBuilder.group({
      fullName: [null, [Validators.required]],
      email: [null, [Validators.required, Validators.pattern("^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[a-zA-Z]{2,}$")]],
      phoneNumber: [null, [Validators.required]],
      schoolId: [this.schoolId],
      inquiry: [null],
    })
  }

  //
  submitForm(): void {
    if (this.validateForm.valid) {
      const formPayload = this.validateForm.value;
      console.log("😜😜😜 ~ file: request-counselling.component.ts:47 ~ RequestCounsellingComponent ~ submitForm ~ formPayload:", formPayload)
      console.log(this.schoolId)
      this._requestService.createRequest(formPayload).subscribe({
        next: response => {
          this.showSuccess();
          this.callSendMail();
          this.closeModal.nativeElement.click();
        }, error: error => {
          this.showFail();
        }
      })
    } else {
      Object.values(this.validateForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity();
        }
      });
    }
  }
  callSendMail() {
    this._mailService.requestCounsellingEmail(this.validateForm.value).subscribe({
      next: response => {
      },
      error: error => {
      }
    })
  }
  requestClick(): void {
    this._userService.getUser().subscribe({
      next: res => {
        this.validateForm = this._formBuilder.group({
          fullName: [res.fullName, [Validators.required]],
          email: [res.account.email, [Validators.required, Validators.pattern("^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[a-zA-Z]{2,}$")]],
          phoneNumber: [res.phoneNumber, [Validators.required, Validators.pattern(/^(0[35789][0-9]{8}|02[0-9]{9})$/)]],
          schoolId: [this.schoolId],
          inquiry: [null],
        });
      }, error: err => {
        console.log('fail')
      }
    })
  }

  showSuccess() {
    this.toastr.success('Request successfully!');
  }

  showFail() {
    this.toastr.error('Request failed!');
  }


  get phoneNumber() { return this.validateForm.get('phoneNumber'); }
  get email() { return this.validateForm.get('email'); }
  get fullName() { return this.validateForm.get('fullName'); }

}
