import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FeeRangeItemComponent } from './fee-range-item.component';

describe('FeeRangeItemComponent', () => {
  let component: FeeRangeItemComponent;
  let fixture: ComponentFixture<FeeRangeItemComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FeeRangeItemComponent]
    });
    fixture = TestBed.createComponent(FeeRangeItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
