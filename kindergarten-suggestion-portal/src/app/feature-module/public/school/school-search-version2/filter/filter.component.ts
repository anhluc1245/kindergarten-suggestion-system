import {Component, Input, OnInit} from '@angular/core';
import {SchoolSearchService} from "../../../../../service/school-search/school-search.service";
import {ActivatedRoute, Router} from "@angular/router";
import {FormArray, FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {first} from "rxjs";

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {
  schoolTypes: any = '';
  admissionAge: any = '';
  facilities: any[] = [];
  utilities: any [] = [];
  minFee!: number;
  maxFee!: number;
  // rating: any = '';
  filterForm!: FormGroup;
  @Input()
  school: any;

  constructor(private _schoolService: SchoolSearchService,
              private _router: Router,
              private _activeRoute: ActivatedRoute,
              private _fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.loadSchoolTypes();
    this.loadAgeOptions();
    this.loadFacility();
    this.loadUtility();
    this.loadMinFee();
    this.loadMaxFee();
    this.filterForm = this._fb.group({
      type: [],
      age: [],
      minFee: [this.minFee],
      maxFee: [this.maxFee],
      rating: this._fb.array([]),
      facilities: this._fb.array([]),
      utilities: this._fb.array([]),
    })

    this._activeRoute.queryParams.pipe(first()).subscribe(params => {
      for (let controlName in this.filterForm.controls) {
        this.filterForm.get(controlName)?.setValue(params[controlName] || '');
      }
    });

    this._activeRoute.queryParams.pipe(first()).subscribe(params => {
      // Lấy tất cả các giá trị từ URL cho facilities và chuyển thành mảng
      this.bindingValueFromUrlToForm(params, 'utilities');
      this.bindingValueFromUrlToForm(params, 'facilities');
      this.bindingValueFromUrlToForm(params, 'rating');
    });
  }

  bindingValueFromUrlToForm(params: any, fieldName: string) {
    const paramValue = params[fieldName];
    const paramValues = paramValue ? (Array.isArray(paramValue) ? paramValue : [paramValue]) : [];
    const selectedValue = this.filterForm.controls[fieldName] as FormArray;
    paramValues.forEach(value => {
      selectedValue.push(new FormControl(Number(value)));
    });
  }

  loadMinFee() {
    this._schoolService.getMinFee().subscribe({
      next: response => {
        this.minFee = response;
      },
      error: error => {
        console.error('Error loading min Fee:', error);
      }
    });
  }

  loadMaxFee() {
    this._schoolService.getMaxFee().subscribe({
      next: response => {
        this.maxFee = response;
      },
      error: error => {
        console.error('Error loading max Fee:', error);
      }
    });
  }

  // load SchoolTypes tu database
  loadSchoolTypes() {
    this._schoolService.getSchoolTypes().subscribe({
      next: response => {
        this.schoolTypes = response;
      },
      error: error => {
        console.error('Error loading school types:', error);
      }
    });
  }

  // load age tu database
  loadAgeOptions(): void {
    this._schoolService.getAdmissionAge().subscribe({
      next: response => {
        this.admissionAge = response;
      },
      error: error => {
        console.error('Error loading age options:', error);
      }
    });
  }

  loadFacility(): void {
    this._schoolService.getFacility().subscribe({
      next: (response: any[]) => {
        if (response) {
          this.facilities = response.map((item: any) => ({
            id: item.id,
            name: item.name,
            code: item.code,
          }));
        }
      },
      error: (error: any) => {
        console.error('Error loading Facility options:', error);
      }
    });
  }

  loadUtility(): void {
    this._schoolService.getUtility().subscribe({
      next: (response: any[]) => {
        if (response) {
          this.utilities = response.map((item: any) => ({
            id: item.id,
            name: item.name,
            code: item.code,
          }));
        }
      },
      error: (error: any) => {
        console.error('Error loading Facility options:');
      }
    });
  }

  buildUrlCondition(event: any, fieldName: string) {
    if (['schoolTypes', 'admissionAge', 'minFee', 'maxFee'].includes(fieldName)) {
      this._router.navigate([], {
        queryParams: {
          ...this.filterForm.value,
          page: 0,
        },
        queryParamsHandling: "merge",
      })
    } else if (['utilities', 'facilities'].includes(fieldName)) {
      const selectedValue = (this.filterForm.controls[fieldName] as FormArray);
      if (event.target.checked) {
        selectedValue.push(new FormControl(event.target.value));
      } else {
        const index = selectedValue.controls.findIndex(x => x.value == event.target.value);
        selectedValue.removeAt(index);
      }

      this._router.navigate([], {
        queryParams: {
          ...this.filterForm.value,
          page: 0,
        },
        queryParamsHandling: 'merge',
      })
    } else {
      console.log("ff" + fieldName);
      const selectedValue = (this.filterForm.controls[fieldName] as FormArray);
      selectedValue.clear();
      // this.filterForm.get('rating')?.reset();
      if (event.target.checked) {
        selectedValue.push(new FormControl(event.target.value));
      }

      this._router.navigate([], {
        queryParams: {
          ...this.filterForm.value,
          page: 0,
        },
        queryParamsHandling: 'merge',
      });
    }
  }

  get utilitiesOpt(): number[] {
    return this.filterForm.get('utilities')?.value;
  }

  get facilitiesOpt(): number[] {
    return this.filterForm.get('facilities')?.value;
  }

  get ratingOpt(): number[] {
    return this.filterForm.get('rating')?.value;
  }
}
