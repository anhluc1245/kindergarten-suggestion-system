import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {SchoolSearchService} from "../../../../../service/school-search/school-search.service";
import {ActivatedRoute, Router} from "@angular/router";
import {LoadDataLocationService} from "../../../../../service/load-data-location-service/load-data-location.service";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  searchForm!: FormGroup;

  listProvinces: any[] = [];
  // nameSchool: any='';
  // selectedProvince: any = '';
  // selectedDistrict: any = '';
  districtOptions: any[] = [];
  page:number=0;
  // wardOptions: any[] = [];

  constructor(private _fb: FormBuilder,
              private _router: Router,
              private locationService: LoadDataLocationService,
              private _activeRoute: ActivatedRoute) {
  }
  ngOnInit(): void {
    this.locationService.getAllProvince().subscribe({
      next: resp => {
        this.listProvinces = resp;
      },
      error: err => {
        this.listProvinces = [];
      }
    });

    this.searchForm = this._fb.group({
      name: '',
      city: '',
      district: '',
    });

    // this._activeRoute.queryParams.subscribe(params => {
    //   this.searchForm.controls['name'].setValue(params['name']);
    //   this.searchForm.controls['city'].setValue(params['city']);
    //   this.searchForm.controls['district'].setValue(params['district']);
    // });

    this._activeRoute.queryParams.subscribe(params => {
      for (let controlName in this.searchForm.controls) {
        this.searchForm.get(controlName)?.setValue(params[controlName] || '');
      }
    });

    if (this.searchForm.controls['city'].value) {
      // Nếu đã có selectedProvince, lấy dữ liệu cho districtOptions
      this.locationService.getAllDistrictByProvinceID(this.searchForm.controls['city'].value).subscribe(district => {
        this.districtOptions = district;
      });
    }


  }
  onProvinceChange(): void {
    if (!this.searchForm.controls['city'].value) {
      this.districtOptions = [];
    } else {
      this.locationService.getAllDistrictByProvinceID(this.searchForm.controls['city'].value).subscribe(district => {
        this.districtOptions = district;
      });
    }
    this.searchForm.controls['district'].setValue('');
  }
  buildUrlCondition() {
    this._router.navigate([], {
      queryParams: {
        ...this.searchForm.value,
        page: 0,
      },
      queryParamsHandling: 'merge',
    });
  }
}

