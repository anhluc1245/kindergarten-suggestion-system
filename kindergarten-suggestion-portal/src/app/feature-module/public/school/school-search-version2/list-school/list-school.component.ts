import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Params, Router} from "@angular/router";
import {SchoolSearchService} from "../../../../../service/school-search/school-search.service";
import {first} from "rxjs";
import {FormBuilder, FormGroup} from "@angular/forms";


@Component({
  selector: 'app-list-school',
  templateUrl: './list-school.component.html',
  styleUrls: ['./list-school.component.scss']
})
export class ListSchoolComponent implements OnInit {
  activePage: number = 0;
  totalPages: number = 0;
  // currentPage!: number;
  size: number = 10;
  totalElements!: number;
  schoolList: any[] = [];
  sortForm!: FormGroup;

  ///


  constructor(private _schoolService: SchoolSearchService,
              private _router: Router,
              private _activeRoute: ActivatedRoute,
              private _fb: FormBuilder) {

    // Sự kiện lắng nghe router thay đổi:
    _router.events.pipe(first(event => event instanceof NavigationEnd))
      .subscribe({
        next: val => {
          this.loadSchoolData();
          // window.location.reload();
        }
      });
  }

  ngOnInit(): void {
    this.sortForm = this._fb.group({
      sort: 1,
      size:this.size,
    });


    this._activeRoute.queryParams.subscribe(params => {
      const sort = params['sort'] || this.sortForm.controls['sort'].value;
      this.sortForm.controls['sort'].setValue(sort);
      this.sortForm.controls['size'].setValue(this.size);
      this.activePage = Number(params['page']) + 1 || this.activePage;
      this.size = Number(params['size']) || this.size;
    });

    this.totalElements = 0;
    this.loadSchoolData();
  }


  loadSchoolData() {
    this._activeRoute.queryParams.subscribe(params => {
      this._schoolService.getSchoolList(this.activePage, this.size, params).subscribe({
        next: res => {
          this.schoolList = res.content;
          this.totalElements = res.totalElements;
          this.totalPages = res.totalPages;
        }
      })
      // this.activePage = Number(params['page']) + 1 || this.activePage;
      // this.currentPage = this.activePage + 1;
      // this.size = Number(params['size']) || this.size;
    });
  }

  buildUrlCondition(event: any, fieldName: string) {
    this._router.navigate([], {
      queryParams: {
        ...this.sortForm.value,
        page: 0
      },
      queryParamsHandling: "merge",
    })
  }

  // //Paging
  handlePageChange(event: number): void {
    this.activePage = event;
    // this.activePage = this.currentPage - 1;
    const queryParams = {...this._activeRoute.snapshot.queryParams};
    queryParams['page'] = event - 1;
    queryParams['size'] = this.size;
    this._router.navigate([], {
      relativeTo: this._activeRoute,
      queryParams,
      queryParamsHandling: 'merge',
    }).then(r => {
      this.loadSchoolData();
    })
  }

  clearQueryParams() {
    // Xây dựng URL mới chỉ với tham số "page" (hoặc các tham số mặc định khác nếu cần)
    const params: Params = {
      name: [],
      city: [],
      district: [],
      sort: [],
      type: [], // Giá trị mặc định cho type
      age: [], // Giá trị mặc định cho age
      minFee: [], // Giá trị mặc định cho minFee (hoặc giá trị khác nếu cần)
      maxFee: [],
      facilities: [],
      utilities: [],
      rating:[],
      size: 10,
      page: 0, // Đặt tham số "page" về giá trị mặc định (hoặc giá trị khác nếu cần)
    };
    this._router.navigate([], {
      queryParams: params,
      queryParamsHandling: 'merge',
    }).then(() => {
      window.location.reload();
    });
    // this._router.navigate(['/home/school']).then(()=> this.ngOnInit())
  }


  // back to top
  scrollToTop() {
    const topElement = document.getElementById('top');
    if (topElement) {
      topElement.scrollIntoView({ behavior: 'smooth' });
    }
  }
}
