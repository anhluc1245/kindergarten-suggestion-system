import {Component, OnInit} from '@angular/core';
import {LoadDataLocationService} from "../../../../../service/load-data-location-service/load-data-location.service";
import {ActivePerfRecorder} from "@angular/compiler-cli/src/ngtsc/perf";
import {ActivatedRoute, Router} from "@angular/router";
import {SearchComunicationService} from "../../../../../service/search-comunication/search-comunication.service";

@Component({
  selector: 'app-load-jsondata-location',
  templateUrl: './load-jsondata-location.component.html',
  styleUrls: ['./load-jsondata-location.component.scss']
})
export class LoadJsondataLocationComponent implements OnInit {
  provinces: any[] = [];
  searchTerm = '';
  selectedProvince: any = '';
  selectedDistrict: any = '';
  districtOptions: any[] = [];
  wardOptions: any[] = [];
  schoolName: any = '';

  constructor(private locationService: LoadDataLocationService,
              private _searchCommunicationService: SearchComunicationService,
              private _activeRoute :ActivatedRoute,
              private _router: Router) {
  }

  ngOnInit(): void {
    this._searchCommunicationService.getSearchData().subscribe( (searchData) => {
      if (searchData) {
        searchData = this.searchOptions
        // Call your search function here, passing in the searchData
        // this.searchSchools(searchData);
        // Navigate to the search results page
        this._router.navigate(['school'], {
          relativeTo: this._activeRoute,
          queryParams: {...searchData},
          queryParamsHandling: 'merge'
        });
      }
    });
    this.locationService.getAllProvince().subscribe({
      next: resp => {
        this.provinces = resp;
      },
      error: err => {
        this.provinces = [];
      }
    });

  }

  onProvinceChange(): void {
    if (!this.selectedProvince) {
      this.districtOptions = [];

    }
    this.selectedDistrict = '';
    if(this.selectedProvince){
      this.locationService.getAllDistrictByProvinceID(this.selectedProvince).subscribe(district => {
        this.districtOptions = district;
      });
    }
  }

  //hàm chưa dùng tới
  onDistrictChange(): void {

    // if (this.selectedDistrict.length == 0) {
    //   this.wardOptions = [];
    // } else {
    //   this.locationService.getAllWardByDistrictID(this.selectedDistrict).subscribe(ward => {
    //     this.wardOptions = ward;
    //   });
    // }
  }
//khai báo các biến dùng cho tìm kiếm và phân trang
  searchedSchoolList: any[] =[];
  searchOptions: any = {};
  totalPages: number = 0;
  activePage: number = 0;
  size: number =3;
  onSearchClick() {
    // Lấy giá trị name tương ứng với selectedProvince và selectedDistrict
    const selectedProvince = this.selectedProvince;

    const selectedDistrict = this.selectedDistrict


    // Gather the search data from your input fields
    const searchData:any = {};
    searchData.q = this.searchTerm ;
    searchData.location = selectedProvince;
    searchData.district = selectedDistrict;
    console.log(searchData)
    //
    // // Send the search data to the SearchComponent via the service
    this._searchCommunicationService.setSearchData(searchData);
    this._router.navigate(['school'], {
      relativeTo: this._activeRoute,
      queryParams: {...searchData},
      queryParamsHandling: 'merge'
    });

  }

  search() {
    // Sử dụng Router để chuyển trang và truyền tham số query
    this._router.navigate(['/home/school'], { queryParams: { name: this.schoolName } });
  }
}
