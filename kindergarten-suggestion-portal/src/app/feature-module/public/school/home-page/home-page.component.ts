import {Component, OnInit} from '@angular/core';
import {SchoolService} from "../../../../service/school-service/school.service";
import {ImageUtility} from "../../../../environment/displayImage";

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  feedbackList: any[] = [];

  constructor(private _schoolService: SchoolService) {
  }

  ngOnInit(): void {
    this._schoolService.getTopNFeedBack().subscribe((resp: any) => {
      if (resp) {
        this.feedbackList = resp;
      }
    })
  }

  protected readonly ImageUtility = ImageUtility;
}
