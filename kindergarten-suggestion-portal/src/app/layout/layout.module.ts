import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LoginComponent } from "../feature-module/auth/login/login.component";
import { EmptyLayoutComponent } from './empty-layout/empty-layout.component';
import { PrivateLayoutComponent } from './private-layout/private-layout.component';
import { PublicLayoutComponent } from './public-layout/public-layout.component';

import { FooterComponent } from './public-layout/footer/footer.component';
import { HeaderComponent } from './public-layout/header/header.component';

import {RouterLink, RouterModule, RouterOutlet} from '@angular/router';
import { LogoutComponent } from "../feature-module/auth/logout/logout.component";
import {ForgotPasswordComponent} from "../feature-module/auth/forgot-password/forgot-password.component";
import {RegisterComponent} from "../feature-module/auth/register/register.component";



@NgModule({
    declarations: [
        EmptyLayoutComponent,
        PublicLayoutComponent,
        PrivateLayoutComponent,
        HeaderComponent,
        FooterComponent,
    ],
    exports: [
        HeaderComponent,
        FooterComponent
    ],
  imports: [
    CommonModule,
    RouterOutlet,
    RouterLink,
    LoginComponent,
    RegisterComponent,
    ForgotPasswordComponent,
    LogoutComponent,
  ]
})
export class LayoutModule { }
