import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorForbidenComponent } from './error-forbiden.component';

describe('ErrorForbidenComponent', () => {
  let component: ErrorForbidenComponent;
  let fixture: ComponentFixture<ErrorForbidenComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ErrorForbidenComponent]
    });
    fixture = TestBed.createComponent(ErrorForbidenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
