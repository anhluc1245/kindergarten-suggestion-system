import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  constructor(
    private _router: Router){}
  fullName: string | null =null;
  ngOnInit(): void {
    const userData = localStorage.getItem('userData');
    if (userData) {
      const parsedUserData = JSON.parse(userData);
      this.fullName = parsedUserData.fullName;
    }
  }
  navigateToSchoolPage() {
    // Chuyển hướng đến trang "school" khi người dùng nhấp vào liên kết
    this._router.navigate(['/school']);
  }
  reloadPage(){
    this._router.navigate(['/home'])
    window.location.reload();
  }
  scrollAboutUs() {
    const about = document.getElementById('about-us');
    if (about) {
      about.scrollIntoView({behavior: 'smooth', block: "start"});
    }
  }
}
