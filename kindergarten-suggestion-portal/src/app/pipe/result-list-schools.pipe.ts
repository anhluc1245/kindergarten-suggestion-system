import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'resultListSchools',
  standalone: true
})
export class ResultListSchoolsPipe implements PipeTransform {

  transform(totalElements: number): string {
    if (totalElements === 0) {
      return '<span class="text-danger">No matching results were found</span>';
    }
    return `There're  <span class="fs-5 text-primary">${totalElements}</span>  schools that match your search criteria`;
  }
}
