import { Pipe, PipeTransform } from '@angular/core';

import { format } from "date-fns";

@Pipe({
  name: 'dateFormat',
  standalone:true
})
export class DateFormatPipe implements PipeTransform {

  transform(value: string, ...args: any[]): string {
    if (!value) return ''; // Xử lý trường hợp giá trị đầu vào không tồn tại

    // Chuyển đổi định dạng từ "yyyy-MM-ddThh:mm:ss" sang "dd/MM/yyyy hh:mm:ss"
    const date = new Date(value);
    return format(date, 'dd/MM/yyyy hh:mm a');
  }

}
