import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'displayName',
  standalone: true
})
export class ChildReceivingAgeDisplayNamePipe implements PipeTransform {
  transform(value: string): string {
    // Chuyển đổi giá trị từ enum thành displayName
    switch (value) {
      case 'AGE_6M_1Y':
        return "6 month - 1 year";
      case 'AGE_1Y_3Y':
        return "1 - 3 year";
      case 'AGE_3Y_6Y':
        return "3 - 6 year";
      default:
        return '';
    }
  }
}
