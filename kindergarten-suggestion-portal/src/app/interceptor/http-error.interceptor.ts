import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { Observable, catchError, throwError } from 'rxjs';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

  constructor(private _router: Router) {
  }
  API_IGNORE_ERROR = ['/api/account/active'];
  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if (this.shouldIgnoreError(request.url)) {
      // Nếu nằm trong danh sách loại trừ, thì bỏ qua xử lý lỗi và tiếp tục chạy request
      return next.handle(request);
    }
    return next.handle(request).pipe(catchError(error => {
      const errorCode: number = error.status;
      switch (errorCode) {
        case 403:
          this._router.navigateByUrl('/error/403');
          break;
        case 401:
          // this._router.navigateByUrl('/error/401');
          break;
        case 404:
          this._router.navigateByUrl('/error/404');
          break;
        case 500:
          this._router.navigateByUrl('/error/500');
          break;
      }

      return throwError(() => error);
    }));
  }
  private shouldIgnoreError(url: string): boolean {
    return this.API_IGNORE_ERROR.some(ignoreUrl => url.includes(ignoreUrl));
  }
}
