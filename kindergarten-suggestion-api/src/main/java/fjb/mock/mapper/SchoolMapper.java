package fjb.mock.mapper;

import fjb.mock.model.dto.schoolDTO.*;
import fjb.mock.model.entities.FeedBack;
import fjb.mock.model.entities.SchoolEnrollDetail;

import fjb.mock.model.entities.*;
import fjb.mock.model.entities.enumEntity.Facility;
import fjb.mock.model.entities.enumEntity.Utility;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

import java.util.List;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface SchoolMapper {

    // @Mapping(source = "images", target = "schoolImageResponseDTOS") // Difference name
    // @Mapping(target = "phoneNumber", ignore = true) // Ignore field
    SchoolDetailResponseDTO toDTO(School school);
    List<SchoolDetailResponseDTO> toDTO(List<School> school);
    HomeFeedBackDTO toDTO(FeedBack feedBack);
    @Mapping(source = "school.id", target = "school.id")
    SchoolEnrollDetailDTO toDTO(SchoolEnrollDetail schoolEnrollDetail);
    FacilityResponseDTO toDTO(Facility facility);
    UtilityResponseDTO toDTO(Utility utility);

}
