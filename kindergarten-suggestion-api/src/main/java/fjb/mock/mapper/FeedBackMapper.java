package fjb.mock.mapper;

import fjb.mock.model.dto.schoolDTO.SchoolFeedbackDTO;
import fjb.mock.model.dto.userDTO.FeedBackWithUserResponseDTO;
import fjb.mock.model.dto.userDTO.FeedbackEnrolledResponseDTO;
import fjb.mock.model.dto.userDTO.FeedbackResponseDTO;
import fjb.mock.model.entities.FeedBack;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import java.util.List;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface FeedBackMapper {
    FeedbackResponseDTO toDTO(FeedBack feedBack);
    FeedBackWithUserResponseDTO toDTOs(FeedBack feedBack);
    List<FeedbackEnrolledResponseDTO> toDTO(List<FeedBack> feedBacks);
}
