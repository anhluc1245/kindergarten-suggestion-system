package fjb.mock.mapper;

import fjb.mock.model.dto.userDTO.UserDetailResponseDTO;
import fjb.mock.model.entities.User;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface UserDetailMapper {

    UserDetailResponseDTO toDTO(User user);
}
