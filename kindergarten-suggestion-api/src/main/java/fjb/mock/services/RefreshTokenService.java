package fjb.mock.services;

import fjb.mock.model.entities.RefreshToken;

import java.util.List;
import java.util.Optional;

public interface RefreshTokenService {
    void save(RefreshToken refreshToken);
    Optional<RefreshToken> findByAccountIdAndDeletedFalseAndRevokedFalse(Long id);
    Optional<RefreshToken> findByAccountIdAndDeletedFalse(Long id);
    Optional<RefreshToken> findByAccountEmailAndDeletedFalseAndRevokedFalse(String email);
    List<RefreshToken> findAllByDeletedFalseAndRevokedFalse();
    void saveAll(List<RefreshToken> refreshTokenList);
    Optional<RefreshToken> findByDeletedFalseAndRevokedFalseAndRefreshToken(String refreshToken);

}


