package fjb.mock.services;

import java.util.List;
import java.util.Optional;

import fjb.mock.model.appEnum.RequestStatusEnum;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import fjb.mock.model.entities.RequestCounselling;


public interface RequestCounsellingService {
    List<RequestCounselling> findAllByUserIdAndStatusInAndDeletedFalse(Long id, List<RequestStatusEnum> statuses);

    void create(RequestCounselling requestCounselling);

    Optional<RequestCounselling> findById(Long id);

//    Page<RequestCounselling> getPageData(Long id, Pageable pageable);
    Page<RequestCounselling> getPageData(String email, Pageable pageable);

    List<RequestCounselling> findAll(Specification<RequestCounselling> spec, Sort sort);
    Page<RequestCounselling> findAllRequest(Specification<RequestCounselling> spec,Pageable pageable);

    Page<RequestCounselling> findAll(Specification<RequestCounselling> spec, Pageable pageable);

    void cancelRequest(RequestCounselling requestCounselling);

    // //test search api
     Page<RequestCounselling> getPageData(Specification<RequestCounselling> spec,
     Pageable pageable);

    Optional<RequestCounselling> findRequestCounsellingByIdAndDeletedFalse(Long id);
    void updateStatus(RequestCounselling requestCounselling);

}
