package fjb.mock.services;

import fjb.mock.model.entities.enumEntity.Facility;

import java.util.List;

public interface FacilitiesService {
    List<Facility> findAllByDeletedFalse();
    Facility findById(Long id);
}
