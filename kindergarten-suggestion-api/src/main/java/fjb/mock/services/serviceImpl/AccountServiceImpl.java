package fjb.mock.services.serviceImpl;

import java.security.SecureRandom;
import java.util.Optional;

//import org.apache.commons.lang3.RandomStringUtils;
//import org.springframework.beans.factory.annotation.Value;
import fjb.mock.model.entities.RefreshToken;
import fjb.mock.repository.RefreshTokenRepository;
import fjb.mock.services.RefreshTokenService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import fjb.mock.mapper.UserMapper;
import fjb.mock.model.appEnum.UserRoleEnum;
import fjb.mock.model.dto.userDTO.LoginResponseDTO;
import fjb.mock.model.entities.Account;
import fjb.mock.model.entities.User;
import fjb.mock.repository.AccountRepository;
import fjb.mock.repository.UserRepository;
import fjb.mock.security.TokenProvider;
import fjb.mock.services.AccountService;

@Service
public class AccountServiceImpl implements AccountService {
    private final AccountRepository accountRepository;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final UserMapper userMapper;
    private final TokenProvider tokenProvider;
    private final RefreshTokenService refreshTokenService;
    private static final String CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    private static final int PASSWORD_LENGTH = 7;

    public AccountServiceImpl(AccountRepository accountRepository, UserRepository userRepository, PasswordEncoder passwordEncoder, AuthenticationManagerBuilder authenticationManagerBuilder, UserMapper userMapper, TokenProvider tokenProvider, RefreshTokenService refreshTokenService, RefreshTokenRepository refreshTokenRepository) {
        this.accountRepository = accountRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.authenticationManagerBuilder = authenticationManagerBuilder;
        this.userMapper = userMapper;
        this.tokenProvider = tokenProvider;
        this.refreshTokenService = refreshTokenService;
    }

    @Override
    public Optional<Account> findByID(Long id) {
        return accountRepository.findById(id);
    }

    @Override
    public boolean checkExistEmail(String email) {
        return accountRepository.existsAccountByEmailAndDeletedFalse(email);
    }

    @Override
    public boolean checkExistPhone(String phone) {
        return userRepository.existsByPhoneNumberAndDeletedFalse(phone);
    }

    @Override
    public void createNew(Account account, User user) {
        RefreshToken refreshToken = new RefreshToken();
        refreshToken.setRevoked(true);
        refreshToken.setDeleted(false);
        refreshToken.setAccount(account);
        account.setDeleted(false);
        account.setUser(user);
        account.setRefreshToken(refreshToken);
        user.setAccount(account);
        user.setDeleted(false);
        account.setPassword(passwordEncoder.encode(account.getPassword()));
        account.setRole(UserRoleEnum.PARENTS);
        accountRepository.save(account);
    }

    @Override
    public void createNew(Account account) {
        account.getUser().setImageProfile("https://vcdn-thethao.vnecdn.net/2023/09/22/jurgen-klopp-lask-reaction-210-2712-4814-1695344199.jpg");
        account.setDeleted(false);
        account.setPassword(passwordEncoder.encode(account.getPassword()));
        account.setRole(UserRoleEnum.PARENTS);
        accountRepository.save(account);
    }

    @Override
    public Optional<Account> findByEmailAndPasswordAndActiveTrueAndDeletedFalse(String email, String password) {
        return accountRepository.findByEmailAndPasswordAndActiveTrueAndDeletedFalse(email, password);
    }

    @Override
    public Optional<Account> findByEmailAndActiveTrueAndDeletedFalse(String email) {
        return accountRepository.findByEmailAndActiveTrueAndDeletedFalse(email);
    }

    @Override
    public Optional<Account> findByEmailAndActiveFalseAndDeletedFalse(String email) {
        return accountRepository.findByEmailAndActiveFalseAndDeletedFalse(email);
    }


    @Override
    public void update(Account account) {
        account.setPassword(passwordEncoder.encode(account.getPassword()));
        accountRepository.save(account);
    }

    @Override
    public void activeAccount(Account account) {
        accountRepository.save(account);
    }

    @Override
    public void delete(Account account) {
        account.setDeleted(true);
        accountRepository.save(account);
    }



    @Override
    public void createNewByAdmin(Account account) {
        account.setPassword(passwordEncoder.encode(account.getPassword()));//FIXME
        account.setDeleted(false);
        accountRepository.save(account);
    }

    @Override
    public LoginResponseDTO login(String email, String password) {
        try{
            UsernamePasswordAuthenticationToken authenticationToken =
                    new UsernamePasswordAuthenticationToken( email, password);
            Authentication authentication = authenticationManagerBuilder
                    .getObject().authenticate(authenticationToken);
            SecurityContextHolder.getContext().setAuthentication(authentication);

            // Xác thực thành công, tạo mã accessToken
            String accessToken = tokenProvider.generateAccessToken(authentication);
            String refreshToken = tokenProvider.generateRefreshToken(authentication);
            // Lấy Optional User từ account email
            Optional<Account> accountOptional =
                    accountRepository.findByEmailAndActiveTrueAndDeletedFalse(email);
            //Tìm RefreshToken tương ứng với account id
            Optional<RefreshToken> refreshTokenOptional =
                    refreshTokenService.findByAccountIdAndDeletedFalse(accountOptional.get().getId());
            //Set giá trị refreshToken và active true cho RefreshToken vừa tìm được
            refreshTokenOptional.get().setRefreshToken(refreshToken);
            refreshTokenOptional.get().setRevoked(false);
            refreshTokenService.save(refreshTokenOptional.get());
            Optional<User> userOptional = userRepository
                    .findUserByAccountEmailAndDeletedFalse(email);
            LoginResponseDTO loginResponseDTO = userMapper.toDTO(userOptional.get());
            // Kiểm tra optional có giá trị hay không để gán về loginResponseDTO
            loginResponseDTO.setAccessToken(accessToken);
            loginResponseDTO.setRefreshToken(refreshToken);
            return loginResponseDTO;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public LoginResponseDTO refreshToken(String refreshToken) {
        try {
            Authentication authentication =
                    tokenProvider.getAuthentication(refreshToken);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            Optional<RefreshToken> refreshTokenOptional =
                    refreshTokenService.findByDeletedFalseAndRevokedFalseAndRefreshToken(refreshToken);
            String email = refreshTokenOptional.get().getAccount().getEmail();
            String newAccessToken = tokenProvider.generateAccessToken(authentication);
            String newRefreshToken = tokenProvider.generateRefreshToken(authentication);

            refreshTokenOptional.get().setRefreshToken(newRefreshToken);
            refreshTokenService.save(refreshTokenOptional.get());
            Optional<User> userOptional = userRepository
                    .findUserByAccountEmailAndDeletedFalse(email);
            LoginResponseDTO loginResponseDTO = userMapper.toDTO(userOptional.get());
            // Kiểm tra optional có giá trị hay không để gán về loginResponseDTO
            loginResponseDTO.setAccessToken(newAccessToken);
            loginResponseDTO.setRefreshToken(newRefreshToken);
            return loginResponseDTO;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Optional<Account> findAllByRole(UserRoleEnum role) {
        return accountRepository.findAllByRoleAndDeletedFalse(role).stream().findFirst();
    }


    @Override
    public String generateRandomPassword() {
        SecureRandom random = new SecureRandom();
        StringBuilder password = new StringBuilder(PASSWORD_LENGTH);

        for (int i = 0; i < PASSWORD_LENGTH; i++) {
            int randomIndex = random.nextInt(CHARACTERS.length());
            char randomChar = CHARACTERS.charAt(randomIndex);
            password.append(randomChar);
        }
        return password.toString();
    }


}
