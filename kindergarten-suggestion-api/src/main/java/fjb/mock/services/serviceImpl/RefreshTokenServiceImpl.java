package fjb.mock.services.serviceImpl;

import fjb.mock.model.entities.RefreshToken;
import fjb.mock.repository.RefreshTokenRepository;
import fjb.mock.services.RefreshTokenService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RefreshTokenServiceImpl implements RefreshTokenService {
    private final RefreshTokenRepository refreshTokenRepository;

    public RefreshTokenServiceImpl(RefreshTokenRepository refreshTokenRepository) {
        this.refreshTokenRepository = refreshTokenRepository;
    }

    @Override
    public void save(RefreshToken refreshToken) {
        refreshTokenRepository.save(refreshToken);
    }

    @Override
    public Optional<RefreshToken> findByAccountIdAndDeletedFalseAndRevokedFalse(Long id) {
        return refreshTokenRepository.findByAccountIdAndDeletedFalseAndRevokedFalse(id);
    }

    @Override
    public Optional<RefreshToken> findByAccountIdAndDeletedFalse(Long id) {
        return refreshTokenRepository.findByAccountIdAndDeletedFalse(id);
    }

    @Override
    public Optional<RefreshToken> findByAccountEmailAndDeletedFalseAndRevokedFalse(String email) {
        return refreshTokenRepository.findByAccountEmailAndDeletedFalseAndRevokedFalse(email);
    }

    @Override
    public List<RefreshToken> findAllByDeletedFalseAndRevokedFalse() {
        return refreshTokenRepository.findAllByDeletedFalseAndRevokedFalse();
    }

    @Override
    public void saveAll(List<RefreshToken> refreshTokenList) {
        refreshTokenRepository.saveAll(refreshTokenList);
    }

    @Override
    public Optional<RefreshToken> findByDeletedFalseAndRevokedFalseAndRefreshToken(String refreshToken) {
        return refreshTokenRepository.findByDeletedFalseAndRevokedFalseAndRefreshToken(refreshToken);
    }
}
