package fjb.mock.services.serviceImpl;

import fjb.mock.model.entities.enumEntity.Utility;
import fjb.mock.repository.UtilitiesRepository;
import fjb.mock.services.UtilitiesService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class UtilitiesServiceImpl implements UtilitiesService {
    private final UtilitiesRepository utilitiesRepository;

    public UtilitiesServiceImpl(UtilitiesRepository utilitiesRepository) {
        this.utilitiesRepository = utilitiesRepository;
    }

    @Override
    public List<Utility> findAllByDeletedFalse() {
        return utilitiesRepository.findAllByDeletedFalse();
    }

    @Override
    public Utility findById(Long id) {
        return utilitiesRepository.findByIdAndDeletedFalse(id).orElse(null);
    }
}
