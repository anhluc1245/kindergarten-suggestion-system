package fjb.mock.services.serviceImpl;


import fjb.mock.model.entities.FeedBack;
import fjb.mock.model.entities.FeedBack_;
import fjb.mock.model.entities.School;
import fjb.mock.model.entities.School_;
import fjb.mock.repository.FeedBackRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import fjb.mock.services.FeedBackService;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
public class FeedBackServiceImpl implements FeedBackService {
    private final FeedBackRepository feedBackRepository;

    public FeedBackServiceImpl(FeedBackRepository feedBackRepository) {
        this.feedBackRepository = feedBackRepository;
    }

    @Override
    public void createNew(FeedBack feedBack) {
        feedBack.setDeleted(false);
        feedBackRepository.save(feedBack);
    }
    @Override
    public List<FeedBack> getTopFeedback() {
        return feedBackRepository.getTopByDeletedFalse();
    }

    @Override
    public Optional<FeedBack> findBySchoolIdAndUserId(Long id, Long userId) {
        return feedBackRepository.findBySchoolIdAndUserIdAndDeletedFalse(id, userId);
    }
    
    public List<FeedBack> findBySchoolIdAndDeletedFalse(Long id) {
        return feedBackRepository.findBySchoolIdAndDeletedFalse(id);
    }

    @Override
    public Page<FeedBack> findAll(Long id, Specification<FeedBack> spec, Pageable pageable) {
        List<Specification<FeedBack>> specificationList = new ArrayList<>();
        Specification<FeedBack> joinSpec =
                (root, query, criteriaBuilder) -> {
                    Join<School, FeedBack> schoolFeedBackJoin = root.join(FeedBack_.SCHOOL, JoinType.LEFT);
                    return criteriaBuilder.equal(schoolFeedBackJoin.get(School_.ID), id);
                };
        specificationList.add(joinSpec);
        Specification<FeedBack> undeleted =
                (root, query, criteriaBuilder) ->
                        criteriaBuilder.and(criteriaBuilder.equal(root.get("deleted"), false));
        specificationList.add(undeleted);
        specificationList.add(spec);
        spec = specificationList.stream().reduce(Specification::and).orElse(null);
        return feedBackRepository.findAll( spec, pageable);
    }

    @Override
    public List<FeedBack> findFeedbackBySchoolIdAndUserId(Long id, Long userId) {
        return feedBackRepository.findBySchoolIdAndUserId(id, userId);
    }

    @Override
    public Page<FeedBack> findAllBySchoolIdAndDeletedFalse(Long id, Pageable pageable) {
        return feedBackRepository.findAllBySchoolIdAndDeletedFalse(id, pageable);
    }

}
