package fjb.mock.services.serviceImpl;

import fjb.mock.model.entities.enumEntity.Facility;
import fjb.mock.repository.FacilitiesRepository;
import fjb.mock.services.FacilitiesService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FacilitiesServiceImpl implements FacilitiesService {
    private final FacilitiesRepository facilitiesRepository;


    public FacilitiesServiceImpl(FacilitiesRepository facilitiesRepository) {
        this.facilitiesRepository = facilitiesRepository;
    }

    @Override
    public List<Facility> findAllByDeletedFalse() {
        return facilitiesRepository.findAllByDeletedFalse();
    }

    @Override
    public Facility findById(Long id) {
        return facilitiesRepository.findByIdAndDeletedFalse(id).orElse(null);
    }
}
