package fjb.mock.services.serviceImpl;

import fjb.mock.model.entities.School;
import fjb.mock.model.entities.SchoolEnrollDetail;
import fjb.mock.model.entities.User;
import fjb.mock.repository.SchoolEnrollDetailRepository;
import fjb.mock.repository.SchoolRepository;
import fjb.mock.repository.UserRepository;
import fjb.mock.services.SchoolEnrollDetailService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SchoolEnrollDetailServiceImpl implements SchoolEnrollDetailService {
    private final SchoolEnrollDetailRepository schoolEnrollDetailRepository;
    private final UserRepository userRepository;
    private final SchoolRepository schoolRepository;

    public SchoolEnrollDetailServiceImpl(SchoolEnrollDetailRepository schoolEnrollDetailRepository, UserRepository userRepository, SchoolRepository schoolRepository) {
        this.schoolEnrollDetailRepository = schoolEnrollDetailRepository;
        this.userRepository = userRepository;
        this.schoolRepository = schoolRepository;
    }

    @Override
    public Page<SchoolEnrollDetail> findAllByUserIdAndIsEnrollTrueAndStatusAndDeletedFalse(Long id, Boolean status, Pageable pageable) {
        return schoolEnrollDetailRepository.findAllByUserIdAndStatusAndDeletedFalse(id, status, pageable);
    }

    @Override
    public Optional<SchoolEnrollDetail> findByUserIdAndSchoolIdAndDeleteFalse(Long schoolId, Long userId) {
        return schoolEnrollDetailRepository.findBySchoolIdAndUserIdAndDeletedFalse(schoolId, userId);
    }

    @Override
    public ResponseEntity<?> createEnroll(Long userId, Long schoolId) {
        Optional<SchoolEnrollDetail> schoolEnrollDetailOptional =
                schoolEnrollDetailRepository
                        .findBySchoolIdAndUserIdAndDeletedFalse(schoolId, userId);
        Optional<User> userOptional = userRepository.findByIdAndDeletedFalse(userId);
        Optional<School> schoolOptional = schoolRepository.findByIdAndDeletedFalse(schoolId);

        if (userOptional.isEmpty() || schoolOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        if (schoolEnrollDetailOptional.isEmpty()) {
            SchoolEnrollDetail newSchoolEnrollDetail =
                    createNewSchoolEnrollDetail(userOptional.get(), schoolOptional.get());
            schoolEnrollDetailRepository.save(newSchoolEnrollDetail);
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }
        SchoolEnrollDetail schoolEnrollDetail = schoolEnrollDetailOptional.get();
        if (!schoolEnrollDetail.getStatus()) {
            schoolEnrollDetail.setStatus(true);
            schoolEnrollDetailRepository.save(schoolEnrollDetail);
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.noContent().build();
    }

    @Override
    public List<SchoolEnrollDetail> findAllByUserIdAndStatusTrueAndDeletedFalse(Long id) {
        return schoolEnrollDetailRepository.findAllByUserIdAndStatusTrueAndDeletedFalse(id);
    }

    @Override
    public SchoolEnrollDetail createNewSchoolEnrollDetail(User user, School school) {
        SchoolEnrollDetail newSchoolEnrollDetail = new SchoolEnrollDetail();
        newSchoolEnrollDetail.setSchool(school);
        newSchoolEnrollDetail.setUser(user);
        newSchoolEnrollDetail.setDeleted(false);
        newSchoolEnrollDetail.setStatus(true);
        return newSchoolEnrollDetail;
    }
//
//    @Override
//    public Optional<SchoolEnrollDetail> findBySchoolIdAndUserIdAndDeletedFalse(Long schoolId, Long userId) {
//        return schoolEnrollDetailRepository.findBySchoolIdAndUserIdAndDeletedFalse(schoolId,userId);
//    }


}
