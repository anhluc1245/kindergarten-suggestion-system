package fjb.mock.services.serviceImpl;

import fjb.mock.model.entities.SchoolManagement;
import fjb.mock.repository.SchoolManagementRepository;
import fjb.mock.services.SchoolManagementService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SchoolManagementServiceImpl implements SchoolManagementService {
    private final SchoolManagementRepository schoolManagementRepository;

    public SchoolManagementServiceImpl(SchoolManagementRepository schoolManagementRepository) {
        this.schoolManagementRepository = schoolManagementRepository;
    }

    @Override
    public Optional<SchoolManagement> findSchoolManagementBySchoolIdAndUserIdAndDeletedFalse(Long schoolId, Long userId) {
        return schoolManagementRepository.findSchoolManagementBySchoolIdAndUserIdAndDeletedFalse(schoolId,userId);
    }

    @Override
    public List<SchoolManagement> findSchoolManagementByUserIdAndDeletedFalse(Long id) {
        return schoolManagementRepository.findSchoolManagementByUserIdAndDeletedFalse(id);
    }

}
