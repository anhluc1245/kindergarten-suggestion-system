package fjb.mock.services;

import fjb.mock.model.entities.enumEntity.Utility;

import java.util.List;

public interface UtilitiesService {
    List<Utility> findAllByDeletedFalse();
    Utility findById(Long id);
}
