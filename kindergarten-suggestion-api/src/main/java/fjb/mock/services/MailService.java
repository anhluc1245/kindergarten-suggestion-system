package fjb.mock.services;

import jakarta.mail.MessagingException;

public interface MailService {
    public void sendMail(String to,String subject,String text) throws MessagingException;

}
