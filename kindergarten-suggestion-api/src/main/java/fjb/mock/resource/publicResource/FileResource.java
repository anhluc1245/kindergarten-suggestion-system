package fjb.mock.resource.publicResource;

import fjb.mock.services.FileStorageService;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class FileResource {
    private final FileStorageService fileStorageService;
    public FileResource(FileStorageService fileStorageService) {
        this.fileStorageService = fileStorageService;
    }

    @GetMapping("/public/files/**")
    public ResponseEntity<Resource> getFile(HttpServletRequest request) throws IOException {
        String relativePath = request.getServletPath().split("files/")[1];
        Resource resource = fileStorageService.loadFileAsResource(relativePath);
        String mimeType = request.getServletContext()
                .getMimeType(resource.getFile().getAbsolutePath());
        return ResponseEntity
                .ok()
                .contentType(MediaType.parseMediaType(mimeType))
                .body(resource);
    }
}
