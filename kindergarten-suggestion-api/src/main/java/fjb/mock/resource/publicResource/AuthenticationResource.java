package fjb.mock.resource.publicResource;

import java.util.Optional;

import fjb.mock.model.dto.userDTO.*;
import fjb.mock.model.entities.RefreshToken;
import fjb.mock.security.SecurityUtil;
import fjb.mock.security.TokenProvider;
import fjb.mock.services.AccountService;
import fjb.mock.services.RefreshTokenService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fjb.mock.component.SuccessResponse;
import fjb.mock.model.entities.Account;
import fjb.mock.model.entities.User;

@RestController
@RequestMapping("/api/auth")
public class AuthenticationResource {

    private final AccountService accountService;
    private final RefreshTokenService refreshTokenService;
    private final TokenProvider tokenProvider;
    private final UserDetailsService userDetailsService;


    public AuthenticationResource(AccountService accountService, RefreshTokenService refreshTokenService, TokenProvider tokenProvider, UserDetailsService userDetailsService) {
        this.accountService = accountService;
        this.refreshTokenService = refreshTokenService;
        this.tokenProvider = tokenProvider;
        this.userDetailsService = userDetailsService;
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody @Valid LoginRequestDTO loginRequestDTO) {
        Optional<Account> accountOptional = accountService
                .findByEmailAndActiveTrueAndDeletedFalse(loginRequestDTO.getEmail());
        // Nếu không tồn tại thông báo lỗi
        if (accountOptional.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        LoginResponseDTO loginResponseDTO = accountService.login(loginRequestDTO.getEmail(),
                loginRequestDTO.getPassword());
        if (loginResponseDTO == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        return ResponseEntity.ok(loginResponseDTO);
    }


    @PostMapping("/logout")
    public ResponseEntity<?> logout(HttpServletRequest request, HttpServletResponse response) {
        String email = SecurityUtil.getCurrentAccount();
        if (email==null){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication!=null) {
            Optional<RefreshToken> refreshTokenOptional =
                    refreshTokenService.findByAccountEmailAndDeletedFalseAndRevokedFalse(email);
            if (refreshTokenOptional.isPresent()){
                refreshTokenOptional.get().setRevoked(true);
                refreshTokenService.save(refreshTokenOptional.get());
            }
            new SecurityContextLogoutHandler().logout(request, response, authentication);
            SecurityContextHolder.clearContext();
        }
        return ResponseEntity.ok(HttpStatus.ACCEPTED);
    }

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody @Valid RegisterRequestDTO registerRequestDTO,
                                      BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.badRequest().body(bindingResult.getAllErrors());
        }
        if (accountService.checkExistEmail(registerRequestDTO.getEmail())) {
            return ResponseEntity.badRequest().body("Email is existed");
        }
        if (accountService.checkExistPhone(registerRequestDTO.getPhoneNumber())) {
            return ResponseEntity.badRequest().body("Phone Number is existed");
        }

        User user = new User();
        user.setFullName(registerRequestDTO.getFullName());
        user.setPhoneNumber(registerRequestDTO.getPhoneNumber());

        Account account = new Account();
        account.setEmail(registerRequestDTO.getEmail());
        account.setPassword(registerRequestDTO.getPassword());
        account.setActive(false);
        accountService.createNew(account, user);
        return ResponseEntity.ok(new SuccessResponse("Registration successful")); // Trả về thông báo thành công !
    }
    @PostMapping("/refresh")
    public ResponseEntity<?> refreshToken(@RequestBody @Valid RefreshTokenRequestDTO requestDTO){
        if (requestDTO == null || !requestDTO.getGrantType().equals("refreshToken")) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        LoginResponseDTO loginResponseDTO = accountService.refreshToken(requestDTO.getRefreshToken());
        if (loginResponseDTO == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        return ResponseEntity.ok(loginResponseDTO);
    }
}