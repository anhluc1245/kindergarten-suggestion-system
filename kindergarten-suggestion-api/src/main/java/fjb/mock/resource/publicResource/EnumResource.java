package fjb.mock.resource.publicResource;

import fjb.mock.model.appEnum.*;
import fjb.mock.utils.EnumUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/enum")
public class EnumResource {

    @GetMapping  ("/SchoolTypes")
    public ResponseEntity<?> getAllSchoolTypesEnums() {
        List<SchoolTypeEnum> schoolTypeEnums = EnumUtils.getEnumValues(SchoolTypeEnum.class);
        return ResponseEntity.ok(schoolTypeEnums);
    }
    @GetMapping  ("/AdmissionAge")
    public ResponseEntity<?> getAllAdmissionAgeEnums() {
        List<ChildReceivingAgeEnum> childReceivingAgeEnums = EnumUtils.getEnumValues(ChildReceivingAgeEnum.class);
        return ResponseEntity.ok(childReceivingAgeEnums);
    }
    @GetMapping  ("/EducationMethod")
    public ResponseEntity<?> getEducationMethodEnums() {
        List<EducationMethodEnum> educationMethodEnums = EnumUtils.getEnumValues(EducationMethodEnum.class);
        return ResponseEntity.ok(educationMethodEnums);
    }
    @GetMapping  ("/Facility")
    public ResponseEntity<?> getAllFacilityEnum() {
        List<FacilityEnum> facilityEnums = EnumUtils.getEnumValues(FacilityEnum.class);
        return ResponseEntity.ok(facilityEnums);

    }

    @GetMapping  ("/UtilityEnum")
    public ResponseEntity<?> getAllUtilityEnum() {
        List<UtilityEnum> utilityEnums = EnumUtils.getEnumValues(UtilityEnum.class);
        return ResponseEntity.ok(utilityEnums);
    }
}
