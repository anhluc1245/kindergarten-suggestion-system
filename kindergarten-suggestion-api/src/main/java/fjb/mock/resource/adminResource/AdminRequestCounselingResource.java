package fjb.mock.resource.adminResource;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import fjb.mock.mapper.RequestCounsellingMapper;
import fjb.mock.model.appEnum.RequestStatusEnum;
import fjb.mock.model.appEnum.UserRoleEnum;
import fjb.mock.model.dto.userDTO.RequestCounsellingResponseDTO;
import fjb.mock.model.entities.*;
import fjb.mock.security.SecurityUtil;
import fjb.mock.services.RequestCounsellingService;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fjb.mock.model.dto.userDTO.RequestCounsellingDetailResponseDTO;
import fjb.mock.services.SchoolManagementService;
import fjb.mock.services.UserService;

@RestController
@RequestMapping("/api/admin/request")
public class AdminRequestCounselingResource {
    private final RequestCounsellingService requestCounsellingService;
    private final RequestCounsellingMapper requestCounsellingMapper;

    private final UserService userService;

    private final SchoolManagementService schoolManagementService;


    public AdminRequestCounselingResource(RequestCounsellingService requestCounsellingService, RequestCounsellingMapper requestCounsellingMapper, UserService userService, SchoolManagementService schoolManagementService) {
        this.requestCounsellingService = requestCounsellingService;
        this.requestCounsellingMapper = requestCounsellingMapper;
        this.userService = userService;
        this.schoolManagementService = schoolManagementService;
    }

    @GetMapping("/list")
    public ResponseEntity<?> getAllRequest(@RequestParam(defaultValue = "0", required = false) Integer page,
                                           @RequestParam(defaultValue = "10", required = false) Integer size,
                                           @RequestParam(name = "q") Optional<String> keywordOpt, Sort sort){
        sort = Sort.by(Sort.Direction.DESC, RequestCounselling_.STATUS).and(Sort.by(Sort.Direction.ASC,RequestCounselling_.CREATED_DATE));
        PageRequest pageRequest = PageRequest.of(page,size,sort);
        if (!SecurityUtil.isAdmin() && !SecurityUtil.isSchoolOwner()){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN); //403
        }

        List<Specification<RequestCounselling>> specificationList = new ArrayList<>();
        Specification<RequestCounselling> deletedSpec = (root,query,criteriaBuilder)-> criteriaBuilder.equal(root.get(RequestCounselling_.DELETED),false);
        specificationList.add(deletedSpec);

        Specification<RequestCounselling> roleSpec;
        if (SecurityUtil.isSchoolOwner()){
            String email = SecurityUtil.getCurrentAccount();
            roleSpec = (root,query,criteriaBuilder) -> {
                Join<RequestCounselling, School> schoolJoin = root.join(RequestCounselling_.SCHOOL, JoinType.INNER);
                Join<School,SchoolManagement> schoolManagementJoin = schoolJoin.join(School_.SCHOOL_MANAGEMENT,JoinType.INNER);
                Join<SchoolManagement,User> userJoin = schoolManagementJoin.join(SchoolManagement_.USER,JoinType.INNER);
                Join<User,Account> accountJoin = userJoin.join(User_.ACCOUNT,JoinType.INNER);
                return criteriaBuilder.equal(accountJoin.get(Account_.EMAIL),email);
            };
            specificationList.add(roleSpec);
        }

        Specification<RequestCounselling> spec;
        if (keywordOpt.isPresent()) {
            String keyword = keywordOpt.get();
            spec = (((root, query, criteriaBuilder) -> {
                Predicate predicate = criteriaBuilder.or(
                        criteriaBuilder.like(root.get(User_.FULL_NAME),"%" + keyword + "%"),
                        criteriaBuilder.like(root.get(Account_.EMAIL),"%" + keyword + "%"),
                        criteriaBuilder.like(root.get(User_.PHONE_NUMBER),"%" + keyword + "%"));
                boolean isRequestStatusEnumValue = false;
                for (RequestStatusEnum statusEnum : RequestStatusEnum.values()) {
                    if (statusEnum.name().equalsIgnoreCase(keyword)) {
                        isRequestStatusEnumValue = true;
                        break;
                    }
                }
                if (isRequestStatusEnumValue) {
                    Path<RequestStatusEnum> statusEnumPath = root.get("request").get("status");
                    predicate = criteriaBuilder.or(
                            predicate,
                            criteriaBuilder.equal(statusEnumPath,
                                    RequestStatusEnum.valueOf(keyword.toUpperCase())));
                }
                return predicate;
            }));
            specificationList.add(spec);
        }
        Specification<RequestCounselling> finalSpec = specificationList.stream().reduce(Specification::and).orElse(null);
        Page<RequestCounselling> requestCounsellingList =
                requestCounsellingService.findAll(finalSpec,pageRequest);
        Page<RequestCounsellingResponseDTO> requestCounsellingResponseDTOList =
                requestCounsellingList.map(requestCounsellingMapper::toDTO);
        return ResponseEntity.ok().body(requestCounsellingResponseDTOList);
    }


    @GetMapping("/counsellingDetail/{id}")
    public ResponseEntity<?> getCounsellingDetail(@PathVariable(name = "id") Long id){
        Optional<RequestCounselling> requestCounsellingOpt = requestCounsellingService.findRequestCounsellingByIdAndDeletedFalse(id);
        if(requestCounsellingOpt.isEmpty()){
            return ResponseEntity.notFound().build();
        }
        RequestCounsellingDetailResponseDTO requestCounsellingDetailResponseDTO = requestCounsellingMapper.convertDTO(requestCounsellingOpt.get());
        return ResponseEntity.ok(requestCounsellingDetailResponseDTO);
    }

    @PutMapping("/updateStatusCounselling/{id}")
    public ResponseEntity<?> updateStatusCounselling(@PathVariable(name = "id") Long id){
        Optional<RequestCounselling> requestCounsellingOpt = requestCounsellingService.findRequestCounsellingByIdAndDeletedFalse(id);
        if(requestCounsellingOpt.isEmpty()){
            return ResponseEntity.notFound().build();
        }
        Long schoolId =requestCounsellingOpt.get().getSchool().getId();
        String email = SecurityUtil.getCurrentAccount();
        Optional<User> optionalUser = userService.findUserByEmail(email);
        if(optionalUser.isEmpty()){
            return ResponseEntity.notFound().build();
        }
        Long userId = optionalUser.get().getId();

        if(requestCounsellingOpt.get().getStatus().equals(RequestStatusEnum.OPEN)){
            if(SecurityUtil.isAdmin()){
                requestCounsellingService.updateStatus(requestCounsellingOpt.get());
            }else if(SecurityUtil.isSchoolOwner()&&
                    schoolManagementService.findSchoolManagementBySchoolIdAndUserIdAndDeletedFalse(schoolId,userId).isPresent()
            ){
                requestCounsellingService.updateStatus(requestCounsellingOpt.get());
            }else {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }

        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("/listReminder")
    public ResponseEntity<?> getAllCounselling(@RequestParam(required = false, defaultValue = "0") Integer page,
                                               @RequestParam(required = false, defaultValue = "5") Integer size,
                                               @RequestParam(name = "q") Optional<String> keywordOpt) {
        List<Specification<RequestCounselling>> specs = new ArrayList<>();
        UserRoleEnum roleEnum = SecurityUtil.getRoleCurrentAccount();
        if (roleEnum != UserRoleEnum.ADMIN && roleEnum != UserRoleEnum.SCHOOL_OWNER) {
            return new ResponseEntity<>("Access Denied", HttpStatus.FORBIDDEN);
        }
        Specification<RequestCounselling> deletedSpec = (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(RequestCounselling_.DELETED), false);
        specs.add(deletedSpec);
        Specification<RequestCounselling> roleSpec;
        if (roleEnum == UserRoleEnum.ADMIN) {
            roleSpec = (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(RequestCounselling_.STATUS), RequestStatusEnum.OPEN);
            specs.add(roleSpec);
        } else {
            String email = SecurityUtil.getCurrentAccount();
            Specification<RequestCounselling> specStatus;
            specStatus = (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(RequestCounselling_.STATUS), RequestStatusEnum.OPEN);
            specs.add(specStatus);

            roleSpec = (root, query, criteriaBuilder) ->
            {
                Join<RequestCounselling, School> requestSchoolJoin = root.join(RequestCounselling_.SCHOOL, JoinType.INNER);
                Join<School, SchoolManagement> schoolManagementJoin = requestSchoolJoin.join(School_.SCHOOL_MANAGEMENT, JoinType.INNER);
                Join<SchoolManagement, User> schoolManagementUserJoin = schoolManagementJoin.join(SchoolManagement_.user, JoinType.INNER);
                Join<User, Account> userAcountJoin = schoolManagementUserJoin.join(User_.ACCOUNT, JoinType.INNER);
                return criteriaBuilder.equal(userAcountJoin.get(RequestCounselling_.EMAIL), email);
            };
            specs.add(roleSpec);
        }


        Specification<RequestCounselling> spec = null;
        if (keywordOpt.isPresent()) {
            String keyword = keywordOpt.get();
            spec = (root, query, criteriaBuilder) ->
                    criteriaBuilder.or(
                            criteriaBuilder.like(root.get(RequestCounselling_.FULL_NAME), "%" + keyword + "%"),
                            criteriaBuilder.like(root.get(RequestCounselling_.PHONE_NUMBER), "%" + keyword + "%"),
                            criteriaBuilder.like(root.get(RequestCounselling_.EMAIL), "%" + keyword + "%"));
            specs.add(spec);
        }

        Specification<RequestCounselling> combinedSpec = specs.stream().reduce(Specification::and).orElse(null);//
        //
        Sort sort = Sort.by(Sort.Order.desc(RequestCounselling_.ID), Sort.Order.desc(RequestCounselling_.FULL_NAME));
        PageRequest pageRequest = PageRequest.of(page, size, sort);
        Page<RequestCounselling> requestCounsellings = requestCounsellingService.findAllRequest(combinedSpec, pageRequest);
        Page<RequestCounsellingResponseDTO> requestCounsellingResponseDTOList = requestCounsellings.map(requestCounsellingMapper::toDTO);
        return ResponseEntity.ok(requestCounsellingResponseDTOList);
    }
}