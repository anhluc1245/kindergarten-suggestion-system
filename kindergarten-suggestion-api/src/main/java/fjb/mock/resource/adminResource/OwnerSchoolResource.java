package fjb.mock.resource.adminResource;

import fjb.mock.mapper.AdminSchoolMapper;
import fjb.mock.model.appEnum.SchoolStatusEnum;
import fjb.mock.model.appEnum.UserRoleEnum;
import fjb.mock.model.dto.schoolDTO.SchoolImageResponseDTO;
import fjb.mock.model.dto.schoolDTO.adminSchoolDTO.AdminSchoolRequestDTO;
import fjb.mock.model.dto.schoolDTO.adminSchoolDTO.AdminSchoolResponseDTO;
import fjb.mock.model.dto.schoolDTO.adminSchoolDTO.SchoolListResponseDTO;
import fjb.mock.model.entities.*;
import fjb.mock.security.SecurityUtil;
import fjb.mock.services.*;
import jakarta.persistence.criteria.*;
import jakarta.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/owner/school")
public class OwnerSchoolResource extends BaseResource {
    private final String folderImage = "school";
    private final SchoolService schoolService;
    private final AdminSchoolMapper adminSchoolMapper;
    private final UserService userService;
    private final FileStorageService fileStorageService;
    private final ImageEntityService imageEntityService;
    private final AddressService addressService;

    public OwnerSchoolResource(SchoolService schoolService,
                               AdminSchoolMapper adminSchoolMapper,
                               FileStorageService fileStorageService,
                               UserService userService,
                               ImageEntityService imageEntityService,
                               AddressService addressService,
                               FacilitiesService facilitiesService,
                               UtilitiesService utilitiesService) {
        super(utilitiesService, facilitiesService);
        this.schoolService = schoolService;
        this.adminSchoolMapper = adminSchoolMapper;
        this.userService = userService;
        this.fileStorageService = fileStorageService;
        this.imageEntityService = imageEntityService;
        this.addressService = addressService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getSchoolById(@PathVariable Long id) {
        Optional<School> schoolOptional = schoolService.findBySchoolIdAndDeletedFalse(id);
        if (schoolOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        School school = schoolOptional.get();
        String email = SecurityUtil.getCurrentAccount();
        if (school.getSchoolManagement() == null || SecurityUtil.isSchoolOwner()
                && !school.getSchoolManagement().getUser().getAccount().getEmail().equals(email)) {
            return ResponseEntity.status(HttpStatusCode.valueOf(403)).build();
        }
        AdminSchoolResponseDTO schoolDTO = adminSchoolMapper.toDTO(school);
        schoolDTO.setOwnerId(school.getSchoolManagement().getUser().getId());
        ImageEntity mainImage = school.getImages().stream().filter(ImageEntity::getIsMainImage).findFirst().orElse(new ImageEntity());
        schoolDTO.setImages(schoolDTO.getImages().stream().filter(img -> !img.getIsMainImage()).collect(Collectors.toList()));
        SchoolImageResponseDTO mainImageDTO = new SchoolImageResponseDTO();
        BeanUtils.copyProperties(mainImage, mainImageDTO);
        schoolDTO.setMainImage(mainImageDTO);
        return ResponseEntity.ok(schoolDTO);
    }

    @PatchMapping("/submit/{id}")
    public ResponseEntity<?> submitSchool(@PathVariable Long id) {
        Optional<School> schoolOptional = schoolService.findBySchoolIdAndDeletedFalse(id);
        if (schoolOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        School school = schoolOptional.get();
        if (school.getStatus() != SchoolStatusEnum.SAVED && school.getStatus() != SchoolStatusEnum.REJECTED) {
            return ResponseEntity.badRequest().build();
        }
        school.setStatus(SchoolStatusEnum.SUBMITTED);
        schoolService.save(school);
        return ResponseEntity.noContent().build();
    }


    @PatchMapping("/publish/{id}")
    public ResponseEntity<?> publishSchool(@PathVariable Long id) {
        Optional<School> schoolOptional = schoolService.findBySchoolIdAndDeletedFalse(id);
        if (schoolOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        School school = schoolOptional.get();
        if (school.getStatus() != SchoolStatusEnum.APPROVED && school.getStatus() != SchoolStatusEnum.UNPUBLISHED) {
            return ResponseEntity.badRequest().build();
        }
        school.setStatus(SchoolStatusEnum.PUBLISHED);
        schoolService.save(school);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/unPublish/{id}")
    public ResponseEntity<?> unPublishSchool(@PathVariable Long id) {
        Optional<School> schoolOptional = schoolService.findBySchoolIdAndDeletedFalse(id);
        if (schoolOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        School school = schoolOptional.get();
        if (school.getStatus() != SchoolStatusEnum.PUBLISHED) {
            return ResponseEntity.badRequest().build();
        }
        school.setStatus(SchoolStatusEnum.UNPUBLISHED);
        schoolService.save(school);
        return ResponseEntity.noContent().build();
    }

    @PutMapping(value = "/submit", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> submitEditSchool(@RequestPart(name = "school") @Valid AdminSchoolRequestDTO schoolRequest,
                                              @RequestPart(name = "mainImage", required = false) MultipartFile mainImage,
                                              @RequestPart(name = "images", required = false) MultipartFile[] files) throws IOException {

        return commonPut(schoolRequest, mainImage, files, SchoolStatusEnum.SUBMITTED);
    }

    @PutMapping(value = "/saved", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> savedEditSchool(@RequestPart(name = "school") @Valid AdminSchoolRequestDTO schoolRequest,
                                             @RequestPart(name = "mainImage", required = false) MultipartFile mainImage,
                                             @RequestPart(name = "images", required = false) MultipartFile[] files) throws IOException {

        return commonPut(schoolRequest, mainImage, files, SchoolStatusEnum.SAVED);
    }

    public ResponseEntity<?> commonPut(AdminSchoolRequestDTO schoolRequest,
                                       MultipartFile mainImage,
                                       MultipartFile[] files, SchoolStatusEnum status) throws IOException {
        Long schoolIdRequest = schoolRequest.getId();
        if (schoolIdRequest == null) {
            return ResponseEntity.notFound().build();
        }
        ResponseEntity<?> validateMainImg = imageEntityService.validateImage(mainImage);
        if (validateMainImg != null) {
            return validateMainImg;
        }
        if (files != null) {
            for (MultipartFile file : files) {
                ResponseEntity<?> validateOtherImg = imageEntityService.validateImage(file);
                if (validateOtherImg != null) {
                    return validateOtherImg;
                }
            }
        }
        Optional<School> oldSchoolOptional = schoolService.findBySchoolIdAndDeletedFalse(schoolIdRequest);
        if (oldSchoolOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        School oldSchool = oldSchoolOptional.get();
        // check role put
        if (SecurityUtil.isSchoolOwner()) {
            String email = SecurityUtil.getCurrentAccount();
            boolean isSchoolManager =
                    oldSchool.getSchoolManagement().getUser().getAccount().getEmail().equals(email);
            if (!isSchoolManager) {
                return ResponseEntity.status(HttpStatusCode.valueOf(403)).build();
            }
        }
        School school = adminSchoolMapper.toEntity(schoolRequest);

        copyUnchangedProperties(school, oldSchool);
        // Address
        ResponseEntity<?> setAddress = addressService.setAddress(school, schoolRequest);
        if (setAddress != null) {
            return setAddress;
        }

        oldSchool.getSchoolFacilities().forEach(schoolFacilities -> schoolFacilities.setDeleted(true));
        setSchoolFacilities(school, schoolRequest);

        oldSchool.getSchoolUtilities().forEach(schoolUtilities -> schoolUtilities.setDeleted(true));
        setSchoolUtilities(school, schoolRequest);

        // image
        school.setImages(imageEntityService.handleUpdateImage(schoolRequest, oldSchool, mainImage, files, folderImage));

        school.setStatus(status);
        school.setDeleted(false);
        schoolService.save(school);
        return ResponseEntity.noContent().build();
    }

    @PostMapping(value = "/submit", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> submitNewSchool(@RequestPart(name = "school") @Valid AdminSchoolRequestDTO schoolRequest,
                                             @RequestPart(name = "mainImage", required = false) MultipartFile mainImage,
                                             @RequestPart(name = "images", required = false) MultipartFile[] files) throws IOException {

        return commonPost(schoolRequest, mainImage, files, SchoolStatusEnum.SUBMITTED);
    }

    @PostMapping(value = "/saved", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> createNewSchool(@RequestPart(name = "school") @Valid AdminSchoolRequestDTO schoolRequest,
                                             @RequestPart(name = "mainImage", required = false) MultipartFile mainImage,
                                             @RequestPart(name = "images", required = false) MultipartFile[] files) throws IOException {
        return commonPost(schoolRequest, mainImage, files, SchoolStatusEnum.SAVED);
    }

    public ResponseEntity<?> commonPost(AdminSchoolRequestDTO schoolRequest,
                                        MultipartFile mainImage,
                                        MultipartFile[] files, SchoolStatusEnum status) throws IOException {
        if (schoolRequest.getId() != null) {
            return ResponseEntity.badRequest().build();
        }
        ResponseEntity<?> validateMainImg = imageEntityService.validateImage(mainImage);
        if (validateMainImg != null) {
            return validateMainImg;
        }
        if (files != null) {
            for (MultipartFile file : files) {
                ResponseEntity<?> validateOtherImg = imageEntityService.validateImage(file);
                if (validateOtherImg != null) {
                    return validateOtherImg;
                }
            }
        }
        User owner;
        if (SecurityUtil.isAdmin()) {
            if (schoolRequest.getOwnerId() == null) {
                return ResponseEntity.badRequest().body("School Owner is required!");
            }
            Optional<User> schoolOwner = userService.findByID(schoolRequest.getOwnerId());
            if (schoolOwner.isEmpty()) {
                return ResponseEntity.badRequest().body("School Owner is not exist!");
            }
            owner = schoolOwner.get();
        } else if (SecurityUtil.isSchoolOwner()) {
            String email = SecurityUtil.getCurrentAccount();
            Optional<User> schoolOwner = userService.findUserByEmail(email);
            if (schoolOwner.isEmpty() || schoolOwner.get().getAccount().getRole() != UserRoleEnum.SCHOOL_OWNER) {
                return ResponseEntity.badRequest().body("School Owner is not exist!");
            }
            owner = schoolOwner.get();
        } else {
            return ResponseEntity.status(HttpStatusCode.valueOf(403)).build();
        }

        School school = adminSchoolMapper.toEntity(schoolRequest);

        // Address
        ResponseEntity<?> setAddress = addressService.setAddress(school, schoolRequest);
        if (setAddress != null) {
            return setAddress;
        }

        setSchoolFacilities(school, schoolRequest);
        setSchoolUtilities(school, schoolRequest);

        // image
        List<ImageEntity> images = new ArrayList<>();
        if (mainImage != null && !mainImage.isEmpty()) {
            String filePath = fileStorageService.saveFile(mainImage, folderImage);
            ImageEntity imageEntity = new ImageEntity();
            imageEntity.setSchool(school);
            imageEntity.setIsMainImage(true);
            imageEntity.setFilePath(filePath);
            imageEntity.setDeleted(false);
            images.add(imageEntity);
        }
        if (files != null) {
            for (MultipartFile file : files) {
                if (file.isEmpty()) {
                    break;
                }
                String filePath = fileStorageService.saveFile(file, folderImage);
                ImageEntity imageEntity = new ImageEntity();
                imageEntity.setSchool(school);
                imageEntity.setIsMainImage(false);
                imageEntity.setFilePath(filePath);
                imageEntity.setDeleted(false);
                images.add(imageEntity);
            }
        }
        school.setImages(images);
        school.setStatus(status);

        SchoolManagement schoolManagement = new SchoolManagement();
        schoolManagement.setUser(owner);
        schoolManagement.setSchool(school);
        schoolManagement.setDeleted(false);
        school.setSchoolManagement(schoolManagement);
        school.setDeleted(false);
        return new ResponseEntity<>(adminSchoolMapper.toDTO(schoolService.save(school)), HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteSchool(@PathVariable Long id) {
        Optional<School> schoolOptional = schoolService.findBySchoolIdAndDeletedFalse(id);
        if (schoolOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        School school = schoolOptional.get();
        // check role delete
        if (SecurityUtil.isSchoolOwner()) {
            String email = SecurityUtil.getCurrentAccount();
            boolean isSchoolManager = school.getSchoolManagement().getUser().getAccount().getEmail().equals(email);
            if (!isSchoolManager) {
                return ResponseEntity.status(HttpStatusCode.valueOf(403)).build();
            }
        }
        school.setStatus(SchoolStatusEnum.DELETED);
        school.setDeleted(true);
        schoolService.save(school);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/list")
    public ResponseEntity<?> getAllSchools(@RequestParam(name = "q", required = false) Optional<String> keywordOpt,
                                           @RequestParam(defaultValue = "0") Integer page,
                                           @RequestParam(defaultValue = "10") Integer size) {
        List<Specification<School>> specs = new ArrayList<>();
        UserRoleEnum roleEnum = SecurityUtil.getRoleCurrentAccount();
        if (roleEnum != UserRoleEnum.ADMIN && roleEnum != UserRoleEnum.SCHOOL_OWNER) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);  // 403 Forbidden
        }

        Specification<School> deleteSpec = (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(School_.DELETED), false);
        specs.add(deleteSpec);

        Specification<School> roleSpec;
        if (roleEnum == UserRoleEnum.ADMIN) {
            roleSpec = (root, query, criteriaBuilder) -> criteriaBuilder.notEqual(root.get(School_.STATUS), SchoolStatusEnum.SAVED);
            specs.add(roleSpec);
        } else {
            String email = SecurityUtil.getCurrentAccount();
            roleSpec = (root, query, criteriaBuilder) -> {
                Join<School, SchoolManagement> schoolManagementJoin = root.join(School_.SCHOOL_MANAGEMENT, JoinType.INNER);
                Join<SchoolManagement, User> userJoin = schoolManagementJoin.join(SchoolManagement_.USER, JoinType.INNER);
                Join<User, Account> accountJoin = userJoin.join(User_.ACCOUNT, JoinType.INNER);
                return criteriaBuilder.equal(accountJoin.get(Account_.EMAIL), email);
            };
            specs.add(roleSpec);
        }

        Specification<School> spec;
        if (keywordOpt.isPresent()) {
            String keyword = keywordOpt.get();
            spec = ((root, query, criteriaBuilder) -> {
                Predicate predicate = criteriaBuilder.or(
                        criteriaBuilder.like(root.get(School_.NAME), "%" + keyword + "%"),
                        criteriaBuilder.like(root.get(School_.PHONE_NUMBER), "%" + keyword + "%"),
                        criteriaBuilder.like(root.get(School_.EMAIL), "%" + keyword + "%"),
                        criteriaBuilder.like(root.get(School_.STREET), "%" + keyword + "%")
                );
                List<Join<School, ?>> joinsToCheck = Arrays.asList(
                        root.join(School_.CITY, JoinType.LEFT),
                        root.join(School_.DISTRICT, JoinType.LEFT),
                        root.join(School_.WARD, JoinType.LEFT)
                );
                for (Join<School, ?> join : joinsToCheck) {
                    Path<String> path = join.get("name");
                    Predicate notNull = criteriaBuilder.isNotNull(path);
                    Predicate like = criteriaBuilder.like(path, "%" + keyword + "%");
                    predicate = criteriaBuilder.or(predicate, criteriaBuilder.and(notNull, like));
                }
                boolean isSchoolRoleEnumValue = Arrays.stream(SchoolStatusEnum.values())
                        .anyMatch(statusEnum -> statusEnum.name().equalsIgnoreCase(keyword));
                if (isSchoolRoleEnumValue) {
                    Path<SchoolStatusEnum> statusEnumPath = root.get(School_.STATUS);
                    predicate = criteriaBuilder.or(
                            predicate,
                            criteriaBuilder.equal(statusEnumPath, SchoolStatusEnum.valueOf(keyword.toUpperCase()))
                    );
                }
                return predicate;
            });

            specs.add(spec);
        }

        Specification<School> combinedSpec = specs.stream().reduce(Specification::and).orElse(null);
        Sort sort = Sort.by(Sort.Order.desc(School_.CREATED_DATE));
        PageRequest pageRequest = PageRequest.of(page, size, sort);
        Page<School> schoolList = schoolService.findAll(combinedSpec, pageRequest);
        Page<SchoolListResponseDTO> schoolListResponseDTOList = schoolList.map(adminSchoolMapper::toListDTO);
        return ResponseEntity.ok(schoolListResponseDTOList);
    }

}