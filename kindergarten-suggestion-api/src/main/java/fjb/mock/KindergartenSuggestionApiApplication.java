package fjb.mock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication
@EnableJpaAuditing(auditorAwareRef = "appAuditorAware")
@EnableWebSecurity
@EnableScheduling
public class KindergartenSuggestionApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(KindergartenSuggestionApiApplication.class, args);
	}

}
