package fjb.mock.model.appEnum;

import lombok.Getter;

@Getter
public enum UtilityEnum {
    SCHOOL_BUS("School bus"),
    BREAKFAST("Breakfast"),
    AFTERSCHOOL_CARE("AfterSchool care"),
    SATURDAY_CLASS("Saturday class"),
    HEALTH_CHECK("Health check"),
    PICNIC_ACTIVITIES("Picnic activities"),
    E_CONTACT_BOOK("E-Contact book");

    public final String displayName;

    UtilityEnum(String displayName) {
        this.displayName = displayName;
    }
}
