package fjb.mock.model.appEnum;

import lombok.Getter;

@Getter
public enum SchoolTypeEnum {
    PUBLIC("Public"),
    INTERNATIONAL("International"),
    PRIVATE("Private"),
    SEMI_PUBLIC("Semi-public"),
    INTERNATIONAL_BILINGUAL("International Bilingual");

    public final String displayName;

    SchoolTypeEnum(String displayName) {
        this.displayName = displayName;
    }
}
