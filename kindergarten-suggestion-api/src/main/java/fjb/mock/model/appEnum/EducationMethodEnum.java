package fjb.mock.model.appEnum;

import lombok.Getter;

@Getter
public enum EducationMethodEnum {
    MONTESSORI("Montessori"),
    STEM("STEM"),
    STEINER("Steiner"),
    REGGIO_EMILIA("Reggio Emilia"),
    HIGH_SCOPE("High Scope"),
    SHICHIDA("Shichida"),
    GLENN_DOMAN("Glenn Doman");

    public final String displayName;

    EducationMethodEnum(String displayName) {
        this.displayName = displayName;
    }
}
