package fjb.mock.model.appEnum;

public enum RequestStatusEnum {
    OPEN, CLOSED, CANCELLED
}
