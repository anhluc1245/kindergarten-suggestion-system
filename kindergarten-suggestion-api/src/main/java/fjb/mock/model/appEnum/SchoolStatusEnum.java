package fjb.mock.model.appEnum;

import lombok.Getter;

@Getter
public enum SchoolStatusEnum {
    SAVED("Saved"),
    SUBMITTED("Submitted"),
    APPROVED("Approved"),
    REJECTED("Rejected"),
    PUBLISHED("Published"),
    UNPUBLISHED("Unpublished"),
    DELETED("Deleted");

    public final String displayName;

    SchoolStatusEnum(String displayName) {
        this.displayName = displayName;
    }
}
