package fjb.mock.model.entities;

import fjb.mock.model.appEnum.*;
import fjb.mock.model.local.District;
import fjb.mock.model.local.Province;
import fjb.mock.model.local.Ward;
import jakarta.persistence.*;
import jakarta.validation.constraints.Pattern;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Where;

import java.math.BigInteger;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Entity
@Data
public class School extends BaseEntity {
    private String name;
    @Pattern(regexp = "^(0[35789][0-9]{8}|02[0-9]{9})$")
    private String phoneNumber;
    @Pattern(regexp = "^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[a-zA-Z]{2,}$")
    private String email;
    private Double avgRating;

    @ManyToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Province city;

    @ManyToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private District district;

    @ManyToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Ward ward;

    private String street;

    private String website;
    private BigInteger minFee;
    private BigInteger maxFee;
    @Column(length = 4000)
    private String introduction;

    @Enumerated(EnumType.STRING)
    private SchoolTypeEnum type;
    @Enumerated(EnumType.STRING)
    private ChildReceivingAgeEnum childReceivingAge;
    @Enumerated(EnumType.STRING)
    private EducationMethodEnum educationMethod;
    @Enumerated(EnumType.STRING)
    private SchoolStatusEnum status;

    @OneToMany(mappedBy = "school")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @Where(clause = "deleted = false")
    private List<SchoolEnrollDetail> schoolEnrollDetails;

    @OneToOne(mappedBy = "school", cascade = CascadeType.ALL)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @Where(clause = "deleted = false")
    private SchoolManagement schoolManagement;

    @OneToMany(mappedBy = "school", cascade = CascadeType.ALL)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @Where(clause = "deleted = false")
    private List<ImageEntity> images;

    @OneToMany(mappedBy = "school", cascade = CascadeType.ALL)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @Where(clause = "deleted = false")
    private List<FeedBack> feedBacks;

    @OneToMany(mappedBy = "school")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @Where(clause = "deleted = false")
    private List<RequestCounselling> requestCounsellings;

    @OneToMany(mappedBy = "school", cascade = CascadeType.ALL)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @Where(clause = "deleted = false")
    private List<SchoolFacilities> schoolFacilities;

    @OneToMany(mappedBy = "school", cascade = CascadeType.ALL)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @Where(clause = "deleted = false")
    private List<SchoolUtilities> schoolUtilities;
}