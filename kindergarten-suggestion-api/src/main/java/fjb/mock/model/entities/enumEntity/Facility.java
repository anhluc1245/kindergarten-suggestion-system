package fjb.mock.model.entities.enumEntity;

import fjb.mock.model.appEnum.FacilityEnum;
import fjb.mock.model.entities.BaseEntity;
import fjb.mock.model.entities.SchoolFacilities;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.OneToMany;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Where;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Entity
@Data
public class Facility extends BaseEntity {
    @Enumerated(EnumType.STRING)
    private FacilityEnum code;
    private String name;

    @OneToMany(mappedBy = "facility")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @Where(clause = "deleted = false")
    private List<SchoolFacilities> schoolFacilities;
}
