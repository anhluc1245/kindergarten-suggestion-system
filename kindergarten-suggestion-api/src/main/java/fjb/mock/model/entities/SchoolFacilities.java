package fjb.mock.model.entities;

import fjb.mock.model.entities.enumEntity.Facility;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Entity
@Data
public class SchoolFacilities extends BaseEntity {
    @ManyToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private School school;
    @ManyToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Facility facility;
}
