package fjb.mock.model.entities;

import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class RefreshToken extends BaseEntity{

    private String refreshToken;
    private Boolean revoked;

    @OneToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Account account;
}
