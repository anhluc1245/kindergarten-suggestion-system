package fjb.mock.model.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import lombok.*;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Entity
@Data
@Getter
@Setter
public class SchoolManagement extends BaseEntity {
    @OneToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private School school;
    @ManyToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private User user;
}
