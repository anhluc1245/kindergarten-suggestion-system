package fjb.mock.model.entities;

import java.time.LocalDate;
import java.util.List;

import fjb.mock.model.local.District;
import fjb.mock.model.local.Province;
import fjb.mock.model.local.Ward;
import jakarta.persistence.*;
import jakarta.validation.constraints.Pattern;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Where;


@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Entity
@Data
@NoArgsConstructor
public class User extends BaseEntity {
    private String fullName;
    private LocalDate dOB;
    @Pattern(regexp = "^(0[35789][0-9]{8}|02[0-9]{9})$")
    private String phoneNumber;

    @ManyToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Province city;

    @ManyToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private District district;

    @ManyToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Ward ward;

    private String street;

    private String imageProfile;
    @OneToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Account account;

    @OneToMany(mappedBy = "user")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @Where(clause = "deleted = false")
    private List<SchoolEnrollDetail> schoolEnrollDetails;

    @OneToMany(mappedBy = "user")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @Where(clause = "deleted = false")
    private List<SchoolManagement> schoolManagements;

    @OneToMany(mappedBy = "user")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @Where(clause = "deleted = false")
    private List<FeedBack> feedBacks;

    @OneToMany(mappedBy = "user")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @Where(clause = "deleted = false")
    private List<RequestCounselling> requestCounsellings;
}
