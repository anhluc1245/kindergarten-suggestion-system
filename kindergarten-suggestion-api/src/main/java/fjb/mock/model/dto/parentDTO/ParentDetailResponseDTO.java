package fjb.mock.model.dto.parentDTO;

import fjb.mock.model.dto.addressDTO.DistrictResponseDTO;
import fjb.mock.model.dto.addressDTO.ProvinceResponseDTO;
import fjb.mock.model.dto.addressDTO.WardResponseDTO;
import fjb.mock.model.dto.schoolDTO.SchoolDetailResponseDTO;
import fjb.mock.model.dto.schoolDTO.SchoolEnrollDetailDTO;
import fjb.mock.model.dto.userDTO.AccountResponseDTO;
import fjb.mock.model.dto.userDTO.FeedbackEnrolledResponseDTO;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class ParentDetailResponseDTO {
    private Long id;
    private String fullName;
    private String phoneNumber;
    private LocalDate dOB;
    private ProvinceResponseDTO city;
    private DistrictResponseDTO district;
    private WardResponseDTO ward;
    private String street;
    private AccountResponseDTO account;
    private List<SchoolEnrollDetailDTO> schoolEnrollDetails;
    private List<FeedbackEnrolledResponseDTO> feedBacks;
    private List<SchoolDetailResponseDTO> school;
}
