package fjb.mock.model.dto.userDTO;

import fjb.mock.model.appEnum.UserRoleEnum;

import lombok.Data;

import java.time.LocalDate;
@Data
public class CreateNewUserDTO {

    private String fullName;

    private String email;

    private LocalDate dOB;

    private String phoneNumber;

    private UserRoleEnum role;

    private String active;
}
