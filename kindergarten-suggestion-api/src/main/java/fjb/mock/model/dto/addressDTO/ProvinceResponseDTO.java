package fjb.mock.model.dto.addressDTO;

import lombok.Data;

@Data
public class ProvinceResponseDTO {
    private Long id;
    private String name;

    public ProvinceResponseDTO(Long id) {
        this.id = id;
    }
}
