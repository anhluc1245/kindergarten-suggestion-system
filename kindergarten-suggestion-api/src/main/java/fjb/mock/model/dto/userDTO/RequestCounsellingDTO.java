package fjb.mock.model.dto.userDTO;

import fjb.mock.model.appEnum.RequestStatusEnum;
import jakarta.persistence.Column;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RequestCounsellingDTO {
    @NotBlank
    private String fullName;

    @NotBlank
    private String email;

    @NotBlank
    private String phoneNumber;

    @Column(length = 4000)
    private String inquiry;

    private RequestStatusEnum status;

    private Long schoolId;
}
