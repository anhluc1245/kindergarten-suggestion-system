package fjb.mock.model.dto.userDTO;

import java.time.LocalDate;

import fjb.mock.model.dto.addressDTO.DistrictResponseDTO;
import fjb.mock.model.dto.addressDTO.ProvinceResponseDTO;
import fjb.mock.model.dto.addressDTO.WardResponseDTO;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

@Data
public class UserToFeedBackResponseDTO {
    private String fullName;
    private LocalDate dOB;
    @Pattern(regexp = "^(0[35789][0-9]{8}|02[0-9]{9})$")
    private String phoneNumber;
    private ProvinceResponseDTO city;
    private DistrictResponseDTO district;
    private WardResponseDTO ward;
    private String street;
    private String imageProfile;
}
