package fjb.mock.model.dto.schoolDTO;

import java.math.BigInteger;

import fjb.mock.model.appEnum.ChildReceivingAgeEnum;
import fjb.mock.model.dto.addressDTO.DistrictDTO;
import fjb.mock.model.dto.addressDTO.ProvinceDTO;
import fjb.mock.model.dto.addressDTO.WardDTO;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Data;

@Data
public class SchoolRequestCounsellingResponseDTO {
    private Long id;
    private String name;
    private String phoneNumber;
    private ProvinceDTO city;
    private DistrictDTO district;
    private WardDTO ward;
    private String street;
    private String website;
    @Enumerated(EnumType.STRING)
    private ChildReceivingAgeEnum childReceivingAge;
    private BigInteger minFee;
    private BigInteger maxFee;

    private Integer totalFeedbacks;
    private Double avgRating;
}
