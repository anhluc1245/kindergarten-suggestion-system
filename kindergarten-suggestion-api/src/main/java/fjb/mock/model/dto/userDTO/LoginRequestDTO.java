package fjb.mock.model.dto.userDTO;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.*;

@Data
public class LoginRequestDTO {
    @NotBlank
    @Pattern(regexp = "^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[a-zA-Z]{2,}$")
    private String email;

    @NotBlank
    @Pattern(regexp = "\\S*", message = "Password must not contain spaces!")
    private String password;
}
