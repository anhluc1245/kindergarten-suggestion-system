package fjb.mock.model.dto.schoolDTO;

import lombok.Data;

@Data
public class RequestedSchoolCounsellingDTO {
    private Long id;
    private String name;

}
