package fjb.mock.model.dto.schoolDTO;

import lombok.Data;


@Data
public class SchoolEnrollDetailDTO {
    private SchoolEnrollItemResponseDTO school;
    private Boolean status;
    private Boolean deleted;
}
