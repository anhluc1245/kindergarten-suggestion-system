package fjb.mock.model.dto.schoolDTO;

import lombok.Data;

@Data
public class SchoolFacilitiesResponseDTO {
    private FacilityResponseDTO facility;
}
