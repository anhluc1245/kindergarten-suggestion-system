package fjb.mock.model.dto.addressDTO;

import lombok.Data;

@Data
public class DistrictResponseDTO {
    private Long id;
    private String name;

    public DistrictResponseDTO(Long id) {
        this.id = id;
    }
}
