package fjb.mock.model.dto.schoolDTO.adminSchoolDTO;

import fjb.mock.model.appEnum.SchoolStatusEnum;
import fjb.mock.model.dto.addressDTO.DistrictDTO;
import fjb.mock.model.dto.addressDTO.ProvinceDTO;
import fjb.mock.model.dto.addressDTO.WardDTO;
import lombok.Data;
import java.time.LocalDateTime;
@Data
public class SchoolListResponseDTO {
    private Long id;
    private String name;
    private String phoneNumber;
    private String email;
    private ProvinceDTO city;
    private DistrictDTO district;
    private WardDTO ward;
    private String street;
    private SchoolStatusEnum status;
    private LocalDateTime createdDate;
}
