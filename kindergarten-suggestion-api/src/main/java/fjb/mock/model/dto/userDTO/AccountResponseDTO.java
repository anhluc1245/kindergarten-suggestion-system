package fjb.mock.model.dto.userDTO;

import fjb.mock.model.appEnum.UserRoleEnum;
import lombok.Data;

@Data
public class AccountResponseDTO {
    private Long id;
    private String email;
    private Boolean active;
    private UserRoleEnum role;
}
