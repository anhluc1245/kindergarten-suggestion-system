package fjb.mock.model.dto.userDTO;

import fjb.mock.model.dto.parentDTO.SchoolParentEnrollDTO;
import jakarta.validation.constraints.Size;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class FeedbackEnrolledResponseDTO {
    private Long id;
    private SchoolParentEnrollDTO school;
    private String content;
    @Size(min = 1, max = 5, message = "Rating from 1 to 5!")
    private Integer learningProgramRating;
    @Size(min = 1, max = 5, message = "Rating from 1 to 5!")
    private Integer facilityAndUtilityRating;
    @Size(min = 1, max = 5, message = "Rating from 1 to 5!")
    private Integer extracurricularActivityRating;
    @Size(min = 1, max = 5, message = "Rating from 1 to 5!")
    private Integer teacherAndStaffRating;
    @Size(min = 1, max = 5, message = "Rating from 1 to 5!")
    private Integer hygieneAndNutritionRating;
    private Double avgRating;
    private LocalDateTime createdDate;
    private Boolean deleted;
}
