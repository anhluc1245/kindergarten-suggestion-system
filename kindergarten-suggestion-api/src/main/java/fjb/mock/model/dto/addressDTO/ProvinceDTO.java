package fjb.mock.model.dto.addressDTO;

import lombok.Data;


@Data
public class ProvinceDTO {
    private Long id;
    private String name;

    public ProvinceDTO(Long id) {
        this.id = id;
    }
}
