package fjb.mock.model.dto.schoolDTO;

import fjb.mock.model.appEnum.FacilityEnum;
import lombok.Data;

@Data
public class FacilityResponseDTO {
    private Long id;
    private FacilityEnum code;
    private String name;
}
