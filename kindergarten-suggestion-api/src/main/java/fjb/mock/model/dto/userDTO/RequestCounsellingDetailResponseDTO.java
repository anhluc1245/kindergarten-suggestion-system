package fjb.mock.model.dto.userDTO;

import fjb.mock.model.appEnum.RequestStatusEnum;
import fjb.mock.model.dto.parentDTO.ParentCounsellingResponseDTO;
import fjb.mock.model.dto.schoolDTO.RequestedSchoolCounsellingDTO;
import lombok.Data;

@Data
public class RequestCounsellingDetailResponseDTO {
//    private Long id;
    private String fullName;

    private String email;

    private String phoneNumber;

    private RequestStatusEnum status;

    private ParentCounsellingResponseDTO user;

    private RequestedSchoolCounsellingDTO school;


    private String inquiry;

}
