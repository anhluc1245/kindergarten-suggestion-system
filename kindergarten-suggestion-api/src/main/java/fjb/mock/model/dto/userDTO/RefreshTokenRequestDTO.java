package fjb.mock.model.dto.userDTO;

import lombok.Data;

@Data
public class RefreshTokenRequestDTO {
    private String grantType;
    private String refreshToken;
}
