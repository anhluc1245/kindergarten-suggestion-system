package fjb.mock.model.dto.schoolDTO.adminSchoolDTO;

import fjb.mock.model.appEnum.ChildReceivingAgeEnum;
import fjb.mock.model.appEnum.EducationMethodEnum;
import fjb.mock.model.appEnum.SchoolTypeEnum;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.math.BigInteger;
import java.util.List;

@Data
public class AdminSchoolRequestDTO {
    private Long id; // School ID
    private Long ownerId; // SchoolOwner ID (UserID)
    @NotBlank
    private String name;
    @Pattern(regexp = "^(0[35789][0-9]{8}|02[0-9]{9})$")
    private String phoneNumber;
    @Pattern(regexp = "^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[a-zA-Z]{2,}$")
    private String email;
    private Long cityId; // city id
    private Long districtId; // district id
    private Long wardId; // ward id
    private String street;

    private String website;
    private BigInteger minFee;
    private BigInteger maxFee;
    @Length(max = 4000)
    @NotBlank
    private String introduction;

    private SchoolTypeEnum type;
    private ChildReceivingAgeEnum childReceivingAge;
    private EducationMethodEnum educationMethod;

    private List<Long> schoolFacilitiesId; // List facility id
    private List<Long> schoolUtilitiesId; // List utility id

    private List<Long> oldImagesId; // [135, 136]
    private Long oldMainImageId; // 132
}
