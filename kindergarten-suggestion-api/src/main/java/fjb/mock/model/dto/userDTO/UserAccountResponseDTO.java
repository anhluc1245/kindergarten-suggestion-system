
package fjb.mock.model.dto.userDTO;

import fjb.mock.model.dto.addressDTO.DistrictResponseDTO;
import fjb.mock.model.dto.addressDTO.ProvinceResponseDTO;
import fjb.mock.model.dto.addressDTO.WardResponseDTO;
import lombok.Data;

import java.time.LocalDate;
@Data
public class UserAccountResponseDTO {
    private Long id;

    private String fullName;

    private LocalDate dOB;

    private String email;

    private String phoneNumber;

    private ProvinceResponseDTO city;
    private DistrictResponseDTO district;
    private WardResponseDTO ward;

    private String street;
}
