package fjb.mock.model.dto.userDTO;

import fjb.mock.model.dto.addressDTO.DistrictResponseDTO;
import fjb.mock.model.dto.addressDTO.ProvinceResponseDTO;
import fjb.mock.model.dto.addressDTO.WardResponseDTO;
import lombok.Data;

import java.time.LocalDate;

@Data
public class UserResponseDTO {
    private Long id;

    private String fullName;

    private String phoneNumber;

    private LocalDate dOB;

    private ProvinceResponseDTO city;

    private DistrictResponseDTO district;

    private WardResponseDTO ward;

    private String street;

    private AccountResponseDTO account;

}
