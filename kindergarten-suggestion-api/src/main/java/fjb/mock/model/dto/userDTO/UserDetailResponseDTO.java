package fjb.mock.model.dto.userDTO;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;

@Data
public class UserDetailResponseDTO {
    private String fullName;

    @JsonFormat(pattern = "dd-MM-yyyy")
    private LocalDate dOB;
    private String phoneNumber;

    private AccountResponseDTO account;
}
