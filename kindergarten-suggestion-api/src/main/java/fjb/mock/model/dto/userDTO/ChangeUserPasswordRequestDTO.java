package fjb.mock.model.dto.userDTO;

import lombok.*;

@Data
public class ChangeUserPasswordRequestDTO {

    private Long id;

    private String currentPassword;

    private String newPassword;

    private String confirmPassword;
}
