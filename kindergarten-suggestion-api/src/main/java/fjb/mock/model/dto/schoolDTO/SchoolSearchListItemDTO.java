package fjb.mock.model.dto.schoolDTO;

import fjb.mock.model.appEnum.ChildReceivingAgeEnum;
import fjb.mock.model.appEnum.EducationMethodEnum;
import fjb.mock.model.appEnum.SchoolTypeEnum;
import jakarta.persistence.Column;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

import java.math.BigInteger;
import java.util.List;

@Data
public class SchoolSearchListItemDTO {
    private String name;
    @Pattern(regexp = "^(0[35789][0-9]{8}|02[0-9]{9})$")
    private String phoneNumber;
    @Pattern(regexp = "^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[a-zA-Z]{2,}$")
    private String email;
    private String city;
    private String district;
    private String ward;
    private String street;
    @Enumerated(EnumType.STRING)
    private SchoolTypeEnum type;
    @Enumerated(EnumType.STRING)
    private ChildReceivingAgeEnum childReceivingAge;
    @Enumerated(EnumType.STRING)
    private EducationMethodEnum educationMethod;
    private BigInteger minFee;
    private BigInteger maxFee;
    @Column(length = 4000)
    private String introduction;


    private String mainImages;
    private Long totalFeedBack;
    private List<SchoolFacilitiesResponseDTO> schoolFacilities;
    private List<SchoolUtilitiesResponseDTO> schoolUtilities;


}
