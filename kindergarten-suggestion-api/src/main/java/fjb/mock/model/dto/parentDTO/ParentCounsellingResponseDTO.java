package fjb.mock.model.dto.parentDTO;

import fjb.mock.model.dto.addressDTO.DistrictResponseDTO;
import fjb.mock.model.dto.addressDTO.ProvinceResponseDTO;
import fjb.mock.model.dto.addressDTO.WardResponseDTO;
import lombok.Data;

@Data
public class ParentCounsellingResponseDTO {
    private Long id;
    private ProvinceResponseDTO city;

    private DistrictResponseDTO district;

    private WardResponseDTO ward;

    private String street;

}
