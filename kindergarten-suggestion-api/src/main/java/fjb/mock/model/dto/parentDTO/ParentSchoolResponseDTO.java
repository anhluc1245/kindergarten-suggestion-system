package fjb.mock.model.dto.parentDTO;

import fjb.mock.model.dto.schoolDTO.SchoolEnrollItemDTO;
import lombok.Data;

@Data
public class ParentSchoolResponseDTO {
    private SchoolEnrollItemDTO school;
    private Boolean status;
    private Boolean deleted;
}
