package fjb.mock.model.dto.schoolDTO;

import lombok.Data;

@Data
public class SchoolImageResponseDTO {
    private Long id;
    private String filePath;
    private Boolean isMainImage;
}
