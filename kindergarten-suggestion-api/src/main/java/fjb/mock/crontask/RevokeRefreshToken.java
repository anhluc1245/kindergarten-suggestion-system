package fjb.mock.crontask;

import fjb.mock.model.entities.RefreshToken;
import fjb.mock.services.RefreshTokenService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class RevokeRefreshToken {
    private final RefreshTokenService refreshTokenService;

    public RevokeRefreshToken(RefreshTokenService refreshTokenService) {
        this.refreshTokenService = refreshTokenService;
    }

    @Scheduled(cron ="0 0 1 * * *" )//01:00:00 AM daily
    @Transactional
    public void revokeRefreshToken(){
        List<RefreshToken> tokenList = refreshTokenService.findAllByDeletedFalseAndRevokedFalse();
        LocalDateTime expiredTime = LocalDateTime.now();
        tokenList.forEach(refreshToken -> {
            if (refreshToken.getLastModifiedDate().plusHours(24).isBefore(expiredTime)){
                refreshToken.setRevoked(true);
            }
        });
        refreshTokenService.saveAll(tokenList);
    }
}
