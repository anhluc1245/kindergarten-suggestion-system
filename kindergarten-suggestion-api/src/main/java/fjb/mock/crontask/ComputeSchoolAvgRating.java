package fjb.mock.crontask;

import java.util.List;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fjb.mock.model.appEnum.SchoolStatusEnum;
import fjb.mock.model.entities.FeedBack;
import fjb.mock.model.entities.School;
import fjb.mock.services.SchoolService;

@Service
public class ComputeSchoolAvgRating {
    private final SchoolService schoolService;

    public ComputeSchoolAvgRating(SchoolService schoolService) {
        this.schoolService = schoolService;
    }

    //        @Scheduled(cron = "1 1 1 * * *") // daily
    @Scheduled(cron = "1 * * * * *") // every minutes
    @Transactional
    public void computeSchoolAvgRatingDaily() {
        List<School> schoolList = schoolService.findAllByStatusAndDeletedFalse(SchoolStatusEnum.PUBLISHED);
        schoolList.forEach(school -> {
            double avgRating = school.getFeedBacks()
                    .stream().mapToDouble(FeedBack::getAvgRating)
                    .average().orElse(0d);
            school.setAvgRating(Math.round(avgRating * 10) * 1.0 / 10);
        });
        schoolService.saveAll(schoolList);
    }
}