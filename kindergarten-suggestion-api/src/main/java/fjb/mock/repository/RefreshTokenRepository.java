package fjb.mock.repository;

import fjb.mock.model.entities.Account;
import fjb.mock.model.entities.RefreshToken;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RefreshTokenRepository extends BaseRepository<RefreshToken, Long>{
    Optional<RefreshToken> findByAccountIdAndDeletedFalseAndRevokedFalse(Long id);
    Optional<RefreshToken> findByAccountIdAndDeletedFalse(Long id);
    Optional<RefreshToken> findByAccountEmailAndDeletedFalseAndRevokedFalse(String email);
    List<RefreshToken> findAllByDeletedFalseAndRevokedFalse();
    Optional<RefreshToken> findByDeletedFalseAndRevokedFalseAndRefreshToken(String refreshToken);
}
