package fjb.mock.repository;

import java.util.List;
import java.util.Optional;

import fjb.mock.model.appEnum.UserRoleEnum;
import org.springframework.stereotype.Repository;

import fjb.mock.model.entities.Account;

@Repository
public interface AccountRepository extends BaseRepository<Account, Long>{
    boolean existsAccountByEmailAndDeletedFalse(String email);
    Optional<Account> findByEmailAndPasswordAndActiveTrueAndDeletedFalse(String email, String password);
    Optional<Account> findByEmailAndActiveTrueAndDeletedFalse(String email);
    Optional<Account> findByEmailAndActiveFalseAndDeletedFalse(String email);
    List<Account> findAllByRoleAndDeletedFalse(UserRoleEnum role);

}