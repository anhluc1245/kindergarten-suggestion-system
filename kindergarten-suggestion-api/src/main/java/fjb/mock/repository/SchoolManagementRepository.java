package fjb.mock.repository;

import fjb.mock.model.entities.SchoolManagement;

import java.util.List;
import java.util.Optional;

public interface SchoolManagementRepository extends BaseRepository<SchoolManagement, Long>{
    Optional<SchoolManagement> findSchoolManagementBySchoolIdAndUserIdAndDeletedFalse(Long schoolId, Long userId);

    List<SchoolManagement> findSchoolManagementByUserIdAndDeletedFalse(Long id);
}
