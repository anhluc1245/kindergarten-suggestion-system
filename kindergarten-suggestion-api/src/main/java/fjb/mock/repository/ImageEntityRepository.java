package fjb.mock.repository;

import fjb.mock.model.entities.ImageEntity;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ImageEntityRepository extends BaseRepository<ImageEntity, Long> {
    List<ImageEntity> findAllByIdInAndDeletedFalse(List<Long> ids);
    Optional<ImageEntity> findByIdAndDeletedFalse(Long id);
}
