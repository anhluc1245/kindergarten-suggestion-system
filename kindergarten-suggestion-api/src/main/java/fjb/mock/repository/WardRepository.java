package fjb.mock.repository;

import fjb.mock.model.local.Ward;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WardRepository extends BaseRepository<Ward,Long> {
    List<Ward> getAllByDistrictId(Long id);

}