package fjb.mock.repository;

import fjb.mock.model.local.District;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DistrictRepository extends BaseRepository<District,Long> {
    List<District> getAllByProvinceId(Long id);
}
