package fjb.mock.component;

public interface PasswordEncrypt {
    String encrypt(String password);
}
